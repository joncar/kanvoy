<!-- footer-->
    <footer style="background-image: url('<?= base_url() ?>pic/footer/footer-bg.jpg'); background-position: center;" class="footer footer-fixed">
      <div class="container">
        <div class="row pb-100 pb-md-40">
          <!-- widget footer-->
          <div class="col-md-6 col-sm-12 mb-sm-30">
            <div class="logo-soc clearfix">
              <div class="footer-logo"><a href="index.html"><img src="/img/logo-white.png" data-at2x="/img/logo-white@2x.png" alt></a></div>
            </div>
            <p class="color-g2 mt-10">
              Creada y organizada para para dar servicio especializado a los jóvenes y estudiantes, colaborando con los centros educativos de todo tipo, sus equipos docentes, apas y asociaciones o grupos de alumnos.
            </p>
            <!-- social-->
            <div class="social-link dark">
              <a href="#" class="cws-social fa fa-twitter"></a>
              <a href="#" class="cws-social fa fa-facebook"></a>
              <a href="#" class="cws-social fa fa-google-plus"></a>
              <a href="#" class="cws-social fa fa-linkedin"></a>
            </div>
            <!-- ! social-->
          </div>
          <!-- ! widget footer-->
          <!-- widget footer-->
          <div class="col-md-3 col-sm-6">
            <div class="widget-footer">
              <h4>Central</h4><br>
              <a href="#" rel="tag" class="tag color-g2">
                C/ Condes de Andrade 1 – 1<br/>
                15174 CULLEREDO, A CORUÑA <br/>
                Teléfono: 881 99 23 03 
              </a>
              <h4>Teléfono Atención Padres</h4><br>
              <a href="tel:+34881992303" rel="tag" class="tag color-g2">
                CENTRAL: 881 992 303 
              </a>
              <a href="tel:+34881885390" rel="tag" class="tag color-g2">
                SUCURSAL: 881 88 53 90
              </a>
              
            </div>
          </div>
          <!-- end widget footer-->
          <!-- widget footer-->
          <div class="col-md-3 col-sm-6">
            <div class="widget-footer">
              <h4>Sucursal</h4><br>
              <a href="#" rel="tag" class="tag color-g2">
                Avda. Concepción Arenal 39b, bajo <br/>
                15179 STA. CRUZ, OLEIROS, A CORUÑA<br/>
                Teléfono: 881 88 53 90
              </a>
              <h4>Datos fiscales</h4><br>
              <a href="#" rel="tag" class="tag color-g2">
                 CIF: 32807078Q <br/> 
                 María Belén Muñoz Seijas<br/>
                 Licencia de Turismo XG-386
              </a>
              
            </div>
          </div>
          <!-- end widget footer-->
        </div>
      </div>
      <!-- copyright-->
      <div class="copyright"> 
        <div class="container">
          <div class="row">
            <div class="col-sm-6">
              <p>© Copyright 2017 <span>KANVOY</span> &nbsp;&nbsp;|&nbsp;&nbsp; <a href="<?= base_url('p/aviso-legal') ?>">Aviso legal</a></p>
            </div>
            <div class="col-sm-6 text-right">
                <a href="<?= site_url() ?>" class="footer-nav">Inicio</a>
                <a href="<?= site_url('p/nosotros') ?>">Quienes somos</a>
                <a href="<?= site_url('p/servicios') ?>">Servicios</a>
                <a href="<?= site_url('grupos') ?>" class="mn-has-sub">Grupos</a>
                <a href="<?= site_url('grupo/all') ?>" class="mn-has-sub">Destinos</a>
                <a href="<?= site_url('p/contacto') ?>" class="mn-has-sub">Contacto</a>
            </div>
          </div>
        </div>
      </div>
      <!-- end copyright-->
      <!-- scroll top-->
    </footer>
    <div id="scroll-top"><i class="fa fa-angle-up"></i></div>