    <div class="site-top-panel">
        <div class="container p-relative">
            <div class="row">
                <div class="col-md-6 col-sm-7">
                    <div class="top-left-wrap font-3">
                        <div class="mail-top">
                            <a href="mailto:info@kanvoy.com"> 
                                <i class="flaticon-suntour-email"></i>info@kanvoy.com
                            </a> - 
                            <a href="mailto:oleiros@kanvoy.com">
                                oleiros@kanvoy.com
                            </a>
                        </div>
                        <span>/</span>
                        <div class="tel-top">
                            <a href="tel:881992303"> 
                                <i class="flaticon-suntour-phone"></i>(881)-992-303
                            </a> - 
                            <a href="tel:881885390"> 
                                (881)-885-390
                            </a>
                        </div>
                    </div>
                    <!-- ! lang select wrapper-->
                </div>
                <div class="col-md-6 col-sm-5 text-right">
                    <div class="top-right-wrap">
                        <div class="top-login experiencebtn" style="padding: 0 23px;">
                            <a href="<?= base_url('experience') ?>">
                                EXPERIENCE
                            </a>
                        </div>
                        <div class="top-login">
                            <a href="http://www.kanvoy.es">
                                VACACIONAL
                            </a>
                        </div>
                        <div>
                            <ul>
                                <li class="top-login">
                                    <a href="http://miex.me">MIEX</i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <nav class="main-nav js-stick">
        <div class="full-wrapper relative clearfix container">
            <div class="nav-logo-wrap local-scroll">
                <a href="<?= empty($module)?site_url():site_url('experience') ?>" class="logo">
                    <img src="<?= base_url() ?>img/logo.png">
                </a>
            </div>
            <div class="inner-nav desktop-nav">
                <ul class="clearlist">              
                    <li class="active-li"><a href="<?= site_url() ?>">Inicio</a></li>              
                    <li class="slash">/</li>              
                    <li><a href="<?= site_url('p/nosotros') ?>">Quienes somos</a></li>
                    <li class="slash">/</li>
                    <li><a href="<?= site_url('p/servicios') ?>">Servicios</a></li>
                    <li class="slash">/</li>
                    <li><a href="<?= site_url('blog') ?>">Blog</a></li>
                    <li class="slash">/</li>
                    <li>
                        <a href="<?= site_url('grupos') ?>" class="mn-has-sub">
                            Grupos <i class="fa fa-angle-down button_open"></i>
                        </a>
                        <ul class="mn-sub">
                            <?php $this->db->limit(4); $this->db->order_by('id','DESC'); ?>
                            <?php foreach ($this->db->get('grupos_destinos')->result() as $g): ?>
                            <li><a href="<?= site_url('grupo/'.toURL($g->id.'-'.$g->nombre)) ?>"><?= $g->nombre ?></a></li>
                            <?php endforeach ?>
                        </ul>
                    </li>
                    <li class="slash">/</li>
                    <li>
                        <a href="<?= site_url('grupo/all') ?>" class="mn-has-sub">Destinos
                            <i class="fa fa-angle-down button_open"></i>
                        </a>
                        <ul class="mn-sub">
                            <?php $this->db->limit('5');
                            foreach ($this->db->get('destinos')->result() as $g): ?>
                            <li><a href="<?= site_url('destino/'.toURL($g->id.'-'.$g->destinos_nombre)) ?>"><?= $g->destinos_nombre ?></a></li>              
<?php endforeach ?>
                        </ul>
                    </li>
                    <li class="slash">/</li>
                    <li><a href="<?= site_url('p/contacto') ?>">Contacto</a></li>
                    <li class="search"><a href="#" class="mn-has-sub">Buscar</a>
                        <ul class="search-sub">
                            <li>
                                <div class="container">
                                    <div class="mn-wrap">
                                        <form method="post" class="form">
                                            <div class="search-wrap">
                                                <input type="text" placeholder="Que deseas buscar?" class="form-control search-field"><i class="flaticon-suntour-search search-icon"></i>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="close-button"><span>Buscar</span></div>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>