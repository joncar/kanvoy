<?= $output ?>
<script>
    $("#field-carreras_id, #field-modalidades_id").change(function(e){
       if($(this).val()!=='' && $("#field-sedes_id").val()!==''){
            e.stopPropagation();            
            $.post('ajax_extension/programacion_carreras_id/', {carreras_id:$("#field-carreras_id").val(), sedes_id:$("#field-sedes_id").val(),modalidades_id:$("#field-modalidades_id").val()}, function(data) {
            var $el = $('#field-programacion_carreras_id');
                      var newOptions = data;
                      $el.empty(); // remove old options
                      $el.append($('<option></option>').attr('value', '').text(''));
                      $.each(newOptions, function(key, value) {
                        $el.append($('<option></option>').attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));                            
                      });                      
                      $el.chosen().trigger('liszt:updated');
                      $(".chzn-container").css('width','100%');
            },'json');
       } 
    });
    
    
    
    $(document).on('change','#field-programacion_carreras_id',function(e) {
            if($(this).val()!==''){
                e.stopPropagation();
                var datos = {
                    programacion_carreras_id:$(this).val(),                    
                    modalidades_id:$("#field-modalidades_id").val(),
                    sedes_id:$("#field-sedes_id").val()
                };
                var old = $('#field-plan_estudio_id').val();
                $.post('ajax_extension/plan_estudio_id/',datos, function(data) {					
                var $el = $('#field-plan_estudio_id');
                          var newOptions = data;
                          $el.empty(); // remove old options
                          $el.append($('<option></option>').attr('value', '').text(''));
                          $.each(newOptions, function(key, value) {
                            $el.append($('<option></option>')
                               .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                                if(key==old){
                                    $el.val(old);
                                }
                            });
                          //$el.attr('selectedIndex', '-1');
                          $el.chosen().trigger('liszt:updated');
                          $(".chzn-container").css('width','100%');

                },'json');        
        }
    });
    
    $(document).on('change','#field-plan_estudio_id, #field-cursos_id',function(e) {
            if($("#field-cursos_id").val()!==''){                
                e.stopPropagation();
                var selectedValue = $('#field-plan_estudio_id').val();
                var old = $('#field-programacion_materias_plan_id').val();
                var datos = {
                    plan_estudio:selectedValue,
                    anho_lectivo:$("#field-anho_lectivo").val(),
                    cursos_id:$("#field-cursos_id").val()
                };
                $.post('ajax_extension/programacion_materias_plan_id/',datos,function(data) {                          
                          var $el = $('#field-programacion_materias_plan_id');
                          var newOptions = data;
                          $el.empty(); // remove old options
                          $el.append($('<option></option>').attr('value', '').text(''));
                          $.each(newOptions, function(key, value) {
                            $el.append($('<option></option>')
                               .attr('value', key).text(value.replace(/&(nbsp|amp|quot|lt|gt);/g,' ')));
                                if(key==old){
                                    $el.val(old);
                                }
                            });
                          //$el.attr('selectedIndex', '-1');
                          $el.chosen().trigger('liszt:updated');
                          $(".chzn-container").css('width','100%');

                },'json');        
        }
    });
    <?php if($action=='edit'): ?>
        $("#field-modalidades_id").trigger('change');
        $("#field-programacion_carreras_id").trigger('change');
        $("#field-plan_estudio_id").trigger('change');
    <?php endif ?>   
</script>