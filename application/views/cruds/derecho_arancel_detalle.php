<?= $output ?>
<script>
    $(document).ready(function(){
        $("#field-cant_cuota, #field-monto").change(function(){
            var monto = parseInt($("#field-monto").val());
            var cuota = parseInt($("#field-cant_cuota").val());
            monto = isNaN(monto)?0:monto;
            cuota = isNaN(cuota)?0:cuota;
            $("#field-total_arancel").val(monto*cuota);
        });
    });
</script>