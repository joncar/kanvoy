<!DOCTYPE html>
<html lang="en-us" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">          
        <title><?= empty($title) ? 'Monalco' : $title ?></title>
        <meta name="keywords" content="<?= empty($keywords) ?'': $keywords ?>" />
        <meta name="description" content="<?= empty($keywords) ?'': $description ?>" />     
        <link rel="icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>    
        <link rel="shortcut icon" href="<?= empty($favicon) ?'': base_url().'img/'.$favicon ?>" type="image/x-icon"/>
        <link rel="stylesheet" href="<?= base_url() ?>css/template/contador.css">
        <link href="<?= base_url() ?>js/stocookie/stoCookie.css" rel="stylesheet">

        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/reset.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/bootstrap.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/font-awesome.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/owl.carousel.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/jquery.fancybox.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/fonts/fi/flaticon.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/flexslider.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/main.css">
        <link rel="stylesheet" href="<?= base_url() ?>css/template/indent.css">
        <link rel="stylesheet" href="<?= base_url() ?>rs-plugin/css/settings.css">
        <link rel="stylesheet" href="<?= base_url() ?>rs-plugin/css/layers.css">
        <link rel="stylesheet" href="<?= base_url() ?>rs-plugin/css/navigation.css">

        
        <script src="//code.jquery.com/jquery-1.10.0.js"></script>	
        <script>var URL = '<?= base_url() ?>';</script>	
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">    
    </head>
    <body>   
        <a href="https://wa.me/34690394388?text=<?= urlencode('Hola, quiero contactar con vosotros') ?>" target="_blank" style="font-size:18px;padding:8px 8px;text-decoration:none;background-color:#189D0E;color:white;text-shadow:none;position:fixed;bottom:79px;right:0px;z-index: 1000;"> <i class="fa fa-whatsapp fa-2x"></i> Contáctanos</a>
        <?= $this->load->view($view); ?>      
        <?= $this->load->view('includes/template/footer'); ?>      
        <?= $this->load->view('includes/template/modal'); ?>      
        <?php if(empty($scripts)){$this->load->view('includes/template/scripts');} ?>      
    </body>   
</html>