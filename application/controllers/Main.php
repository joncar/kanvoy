<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->library('redsysAPI');
        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_ALL, 'es_ES', 'Spanish_Spain', 'Spanish');
    }

    public function index() {
        $this->loadView('main');
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    function pages($titulo) {
        $titulo = urldecode(str_replace("-", "+", $titulo));
        if (!empty($titulo)) {
            $pagina = $this->db->get_where('paginas', array('titulo' => $titulo));
            if ($pagina->num_rows() > 0) {
                $this->loadView(array('view' => 'paginas', 'contenido' => $pagina->row()->contenido, 'title' => $titulo));
            } else
                $this->loadView('404');
        }
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    function getHead($page){
        $ajustes = $this->db->get('ajustes')->row();
        //$stocookie = '<link href="'.base_url('js/stocookie/stoCookie.css').'">';        
        $stocookie = '';
        $page = str_replace('</head>',$stocookie.'</head>',$page);
        return $page;
    }

    function getBody($page){
        $ajustes = $this->db->get('ajustes')->row();        
        $stocookie = '<script src="'.base_url('js/stocookie/stoCookie.min.js').'"></script>';
        $stocookie.= html_entity_decode($ajustes->cookies);
        $page = str_replace('</body>',$stocookie.'</body>',$page);
        $page= str_replace('</body>',$ajustes->analytics.'</body>',$page);
        return $page;
    }

    public function loadView($param = array('view' => 'main')) {
        if (is_string($param))
            $param = array('view' => $param);
        $ajustes = $this->db->get('ajustes')->row();
        if(empty($param['title'])){
            $param['title'] = $ajustes->titulo;
        }
        $param['favicon'] = $ajustes->favicon;
        $param['keywords'] = $ajustes->keywords;
        $param['description'] = $ajustes->description;
        $page = $this->load->view('template', $param,true);
        $page = $this->getHead($page);
        $page = $this->getBody($page);
        //$page = $this->traduccion->traducir($page,$_SESSION['lang']);

        //$page = str_replace('data-lang="'.$_SESSION['lang'].'"','class="active"',$page);
        echo $page;
    }



    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }            
        }

    function testMail(){
        correo('joncar.c@gmail.com','TEST','TEST');
    }

}
