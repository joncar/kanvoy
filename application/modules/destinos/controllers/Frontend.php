<?php 
    require_once APPPATH.'/controllers/Main.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function grupos(){
            $grupos = new Bdsource();
            $grupos->limit = array('6','0');            
            if(!empty($_GET['direccion'])){
                $grupos->like('nombre',$_GET['direccion']);
            }
            //$blog->where('idioma',$_SESSION['lang']);
            if(!empty($_GET['page'])){
                $grupos->limit = array(($_GET['page']-1),6);
            }
            $grupos->init('grupos_destinos');
            foreach($this->grupos_destinos->result() as $n=>$b){
                $this->grupos_destinos->row($n)->link = site_url('grupo/'.toURL($b->id.'-'.$b->nombre));
                $this->grupos_destinos->row($n)->portada = base_url('img/grupos/'.$b->portada);
            }
            $totalpages = round($this->db->get_where('grupos_destinos')->num_rows()/6);
            $totalpages = $totalpages==0?'1':$totalpages;
            $this->loadView(
                array(
                    'view'=>'grupos',
                    'detail'=>$this->grupos_destinos,
                    'total_pages'=>$totalpages,
                    'title'=>'Grupos',
            ));
        }
        
        function destinos($x = ''){
            $params = explode('-',$x);            
            $destinos = new Bdsource();
            $destinos->select = 'destinos.*';
            $destinos->join = array('grupos_destinos');
            $destinos->limit = array('6','0'); 
            if(is_numeric($params[0])){
                $destinos->where('grupos_destinos_id',$params[0]);
            }
            $destinos->select = 'destinos.*,grupos_destinos.nombre as grupo, destinos.precio as precio_desde, destinos.destinos_nombre as nombre';
            if(!empty($_GET['grupo'])){
                $destinos->like('nombre',$_GET['grupo']);
            }
            if(!empty($_GET['destino'])){
                $destinos->like('destinos_nombre',$_GET['destino']);
            }
            if(!empty($_GET['min_price'])){
                $destinos->where('precio_desde >=',$_GET['min_price']);
            }
            if(!empty($_GET['max_price'])){
                $destinos->where('precio_hasta <=',$_GET['max_price']);
            }
            //$blog->where('idioma',$_SESSION['lang']);
            if(!empty($_GET['page'])){
                $destinos->limit = array(($_GET['page']-1),6);
            }
            $destinos->init('destinos');
            foreach($this->destinos->result() as $n=>$b){
                $this->destinos->row($n)->link = site_url('destino/'.toURL($b->id.'-'.$b->nombre));
                $this->destinos->row($n)->portada = base_url('img/destinos/'.$b->portada);
            }
            if(is_numeric($params[0])){
               $grupo = $this->db->get_where('grupos_destinos',array('id'=>$params[0]));
               if($grupo->num_rows()>0){
                   $grupo = $grupo->row()->nombre;
               }else{
                   throw new Exception('Página no encontrada',404);
                   die();
               }
            }else{
                $grupo = '';
            }
            $titulo = empty($grupo)?'Grupos':$grupo;
            $totalpages = round($this->db->get_where('destinos')->num_rows()/6);
            $totalpages = $totalpages==0?'1':$totalpages;
            $this->loadView(
                array(
                    'view'=>'destinos',
                    'detail'=>$this->destinos,
                    'total_pages'=>$totalpages,
                    'title'=>$titulo,
                    'grupo'=>$grupo
            ));                        
        }
        
        function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $destinos = new Bdsource();
                $destinos->select = 'destinos.*, destinos.destinos_nombre as nombre';
                $destinos->where('id',$id);                
                $destinos->init('destinos',TRUE);
                $comentarios = new Bdsource();
                $comentarios->where('destinos_id',$this->destinos->id);
                $comentarios->init('destinos_comentarios');
                
                $recomendados = new Bdsource();
                $recomendados->limit = array(3,0);
                $recomendados->select = 'destinos.*, destinos.destinos_nombre as nombre, grupos_destinos.nombre as grupo';
                $recomendados->where('destinos.grupos_destinos_id',$this->destinos->grupos_destinos_id);                
                $recomendados->where('destinos.id !=',$this->destinos->id);                
                $recomendados->join = array('grupos_destinos');
                $recomendados->init('destinos',FALSE,'recomendados');
                foreach($this->recomendados->result() as $n=>$b){
                    $this->recomendados->row($n)->link = site_url('destino/'.toURL($b->id.'-'.$b->destinos_nombre));
                    $this->recomendados->row($n)->foto = base_url('img/destinos/'.$b->portada);
                }
                
                $fotos = new Bdsource();
                $fotos->where('destinos_id',$this->destinos->id);
                $fotos->init('destinos_fotos');
                $this->loadView(
                    array(
                        'view'=>'destino',
                        'detail'=>$this->destinos,
                        'title'=>$this->destinos->nombre,
                        'fotos'=>$this->destinos_fotos,
                        'recomendados'=>$this->recomendados,
                        'comentarios'=>$this->destinos_comentarios
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
        
        function reservar(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nombre','Nombre','required')
                                  ->set_rules('apellido','Apellido','required')
                                  ->set_rules('instituto','Instituto','required')
                                  ->set_rules('email','Email','required|valid_email|is_unique[reservas.email]')
                                  ->set_rules('direccion','Dirección','required')
                                  ->set_rules('ciudad','Ciudad','required')
                                  ->set_rules('cp','CP','required')
                                  ->set_rules('provincia','Provincia','required')
                                  ->set_rules('telefono','Telefono','required')
                                  ->set_rules('destinos_id','Paquete','required');
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    echo $this->error('Captcha Introducido incorrectamente');
                }else{   
                    unset($_POST['g-recaptcha-response']);                 
                    $this->db->insert('reservas_destinos',$_POST);
                    echo $this->success('Su reserva ha sido cargada con éxito <script>document.location.reload()</script>');
                    $this->enviarcorreo((object)$_POST,2,'info@kanvoy.com');
                    $this->enviarcorreo((object)$_POST,3);
                }
            }else{
                echo $this->error($this->form_validation->error_string(),' <script>$("#guardar").attr("disabled",false); </script>');
            }
        }
                
        function comentario($action = '',$id = ''){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nombre','Nombre','required')
                                  ->set_rules('apellido','Apellido','required')
                                  ->set_rules('telefono','Telefono','required')
                                  ->set_rules('titulo','Titulo','required')
                                  ->set_rules('mensaje','Mensaje','required')
                                  ->set_rules('destinos_id','Destino','required');
            if($this->form_validation->run()){
                $_POST['fecha'] = date("Y-m-d");
                $this->db->insert('destinos_comentarios',$_POST);
                echo $this->success('Su comentario ha sido cargado con éxito <script>document.location.reload()</script>');
            }else{
                echo $this->error('Por favor complete todos los parametros solicitados <script>$("#guardar").attr("disabled",false); </script>');
            }
        }                 
    }
?>
