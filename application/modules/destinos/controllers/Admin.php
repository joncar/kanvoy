<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function grupos_destinos($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->set_subject('Grupo de destino');
            $crud->set_field_upload('portada','img/grupos');
            $crud = $crud->render();
            $crud->title = 'Grupos de destinos';
            $this->loadView($crud);
        } 
        
        function destinos($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('portada','img/destinos');
            $crud->set_field_upload('portada_main','img/destinos');
            $crud->set_field_upload('banner','img/destinos');
            $crud->display_as('portada','Portada (720x240)');
            $crud->display_as('portada_main','Portada (480x350)');
            $crud->display_as('banner','Banner (1920x1280)');
            $crud->field_type('mapa','map',array());
            $crud->field_type('actividades','tags');
            $crud->add_action('<i class="fa fa-envelope"></i> Comentarios','',base_url('destinos/admin/destinos_comentarios').'/');
            $crud->add_action('<i class="fa fa-image"></i> Galeria de fotos','',base_url('destinos/admin/destinos_fotos').'/');
            $crud->add_action('<i class="fa fa-image"></i> Reservas','',base_url('destinos/admin/reservas_destinos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        } 
        
        function reservas_destinos($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->where('destinos_id',$action);
            $crud->field_type('destinos_id','hidden',$action);            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function paquetes($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('portada','img/destinos');
            $crud->set_field_upload('portada_main','img/destinos');
            $crud->set_field_upload('banner','img/destinos');
            $crud->display_as('portada','Portada (720x240)');
            $crud->display_as('portada_main','Portada (480x350)');
            $crud->display_as('banner','Banner (1920x1280)');
            $crud->field_type('mapa','map',array());
            $crud->field_type('actividades','tags');
            $crud->add_action('<i class="fa fa-envelope"></i> Comentarios','',base_url('destinos/admin/destinos_comentarios').'/');
            $crud->add_action('<i class="fa fa-image"></i> Galeria de fotos','',base_url('destinos/admin/destinos_fotos').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }     
        
        function destinos_comentarios($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->where('destinos_id',$action);
            $crud->field_type('destinos_id','hidden',$action);            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function destinos_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('destinos_fotos')
                 ->set_url_field('foto')
                 ->set_relation_field('destinos_id')
                 ->set_image_path('img/destinos')
                 ->module = 'destinos';
            $crud = $crud->render();
            $crud->output.= '<p><b>Nota: </b> El tamaño recomendado para las fotos es de 1170x480</p>';
            $crud->title = 'Galería Fotográfica de destino';
            $this->loadView($crud);
        }
    }
?>
