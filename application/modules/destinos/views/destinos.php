<header><?php $this->load->view('includes/template/header'); ?> 
<!-- breadcrumbs start-->
<section style="background-image: url('<?= base_url() ?>pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;" class="breadcrumbs" data-mce-style="background-image: url('<?= base_url() ?>pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;">
<div class="container"><div class="text-left breadcrumbs-item"><a href="#">Inicio</a><i>/</i> <a href="#" class="last"><span>Destinos</span></a><h2><span>Dest</span>inos</h2></div></div></section>
<!-- ! breadcrumbs end--></header>
<div class="content-body">
    <div class="container page">
        <h2 class="title-section mb-5">
            <span>Buscar</span> Destinos <?= $grupo ?>
        </h2>
        <form method="get" action="#" class="form search" id="formsearch">
            <div class="search-hotels mb-40 pattern">
                <div class="tours-container">
                    <div class="tours-box">
                        <div class="tours-search mb-20">                        
                            <div class="tours-calendar divider-skew">
                                <input placeholder="Grupo" value="<?= !empty($_GET['grupo'])?$_GET['grupo']:'' ?>" name="grupo" class="textbox-n" type="text">
                                <i class="flaticon-suntour-map calendar-icon"></i>
                            </div>
                            <div class="tours-calendar divider-skew"> 
                                <input name="destino" <?= !empty($_GET['destino'])?$_GET['destino']:'' ?> placeholder="Destino" class="textbox-n" type="text">
                                <i class="flaticon-suntour-calendar calendar-icon"></i>
                            </div>
                        </div>

                            <div class="row">
                                <div class="col-md-6 clearfix">
                                    <div class="widget-price-slider float-left">                                
                                            <div class="price_slider_wrapper">
                                                <div aria-disabled="false" class="price_slider price_slider_amount ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
                                                    <div class="ui-slider-range ui-widget-header ui-corner-all"></div>
                                                    <a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 0%;">
                                                        <div style="" class="price_label">
                                                            <span class="from">100<sup>€</sup></span>
                                                        </div>
                                                    </a>
                                                    <a href="#" class="ui-slider-handle ui-state-default ui-corner-all" style="left: 100%;">
                                                        <div style="" class="price_label">
                                                            <span class="to">2000<sup>€</sup></span>
                                                        </div>
                                                    </a>
                                                    <div class="ui-slider-range ui-widget-header ui-corner-all" style="left: 0%; width: 100%;"></div>
                                                </div>
                                                <div class="price_slider_amount addon">
                                                    <input id="min_price" name="min_price" value="" data-min="100" placeholder="Min price" style="display: none;" type="text">
                                                    <input id="max_price" name="max_price" value="" data-max="2000" placeholder="Max price" style="display: none;" type="text">
                                                </div>
                                            </div>                                
                                    </div>

                                </div>
                                <div class="col-md-3 col-md-offset-3">
                                    <div class="tours-search">                                
                                        <div class="button-search" id="enviarform">Buscar</div>
                                    </div>
                                </div>
                            </div>                    
                    </div>
                </div>
            </div>
        </form>
        <div class="row">
            <!-- Recomended item-->
            <?php foreach($detail->result() as $d): ?>
                <?php $this->load->view('_item',array('d'=>$d)); ?>
            <?php endforeach ?>
            <?php if($detail->num_rows()==0): ?>
                <div class="col-md-12">
                    No se han encontrado resultados para los parámetros de búsqueda, por favor intente de nuevo
                </div>
            <?php endif ?>
        </div>
    </div>
    <!-- call out section-->
    <section class="page-section pt-90 pb-80 bg-main pattern relative">
        <div class="container">
            <div class="call-out-box clearfix with-icon">
                <div class="row call-out-wrap">
                    <div class="col-md-5">
                        <h6 class="title-section-top gray font-4">subscribite hoy</h6>
                        <h2 class="title-section alt-2"><span>Coje</span> Las últimas ofertas</h2><i class="flaticon-suntour-email call-out-icon"></i>
                    </div>
                    <div class="col-md-7">
                        <form action="php/contacts-process.php" method="post" class="form contact-form mt-10" novalidate="novalidate">
                            <div class="input-container">
                                <input placeholder="Escribe tu email" value="" name="email" class="newsletter-field mb-0 form-row" type="text"><i class="flaticon-suntour-email icon-left"></i>
                                <button type="submit" class="subscribe-submit"><i class="flaticon-suntour-arrow icon-right"></i></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ! call out section	-->
</div>
<script>
    $(function(){
        $("#enviarform").click(function(){
            $("#formsearch").submit();
        });

    });
</script>