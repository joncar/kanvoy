<div class="col-md-6">
    <div class="recom-item border">
        <div class="recom-media">
            <a href="<?= $d->link ?>">
                <div class="pic">
                    <img src="<?= $d->portada ?>" data-at2x="<?= $d->portada ?>" alt="" style="width: 720px;">
                </div>
            </a>
            <div class="location">
                <?= $this->db->get_where('destinos',array('grupos_destinos_id'=>$d->id))->num_rows() ?> Destinos
            </div>
        </div>
        <!-- Recomended Content-->
        <div class="recom-item-body">
            <a href="<?= $d->link ?>">
                <h6 class="blog-title"><?= $d->nombre ?></h6>
            </a>

            <div class="recom-price">
                Desde <span class="font-4"><?= moneda($d->precio_desde) ?>€</span>
            </div>
            <p class="mb-30"><?= $d->descripcion_corta ?></p>
                <a href="<?= base_url() ?>p/contacto.html" class="recom-button">Contactar</a>  
                <a href="<?= $d->link ?>" class="cws-button small alt">Ver Destinos</a> 
                          
        </div>
        <!-- Recomended Image-->
    </div>
</div>

