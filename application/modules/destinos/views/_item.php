<div class="col-md-6">
    <div class="recom-item border">
        <div class="recom-media">
            <a href="<?= $d->link ?>">
                <div class="pic">
                    <img src="<?= $d->portada ?>" data-at2x="<?= $d->portada ?>" alt="" style="width: 720px;">
                </div>
            </a>
            <div class="location">
                <?= $d->grupo ?>
            </div>
        </div>
        <!-- Recomended Content-->
        <div class="recom-item-body">
            <a href="<?= $d->link ?>">
                <h6 class="blog-title"><?= $d->nombre ?></h6>
            </a>

            <div class="recom-price">
                Desde <span class="font-4"><?= moneda($d->precio_desde) ?>€</span>
            </div>
            <p class="mb-30"><?= $d->descripcion_corta ?></p>
                <a href="<?= $d->link ?>" class="recom-button">Leer Más</a>  
                <a href="<?= $d->link ?>" class="cws-button small alt">RESERVAR</a> 
        </div>
        <!-- Recomended Image-->
    </div>
</div>
