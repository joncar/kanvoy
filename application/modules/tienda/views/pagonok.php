<header>
      <?php $this->load->view('includes/template/header'); ?>
      <!-- End Navigation panel-->
      <!-- breadcrumbs start-->
      <section class="breadcrumbs style-2 gray-90">
        <div class="container">
          <div class="text-left breadcrumbs-item">
          	<a href="<?= base_url() ?>">Inicio</a><i>/</i>
          	<a href="<?= base_url('experience') ?>">Experience</a><i>/</i>
          	<a href="<?= base_url('checkout') ?>" class="last">
          		<span>Pago confirmado</span>
          	</a>
            <h2><span>Pago confirmado</span></h2>
          </div>
        </div>
      </section>
      <!-- ! breadcrumbs end-->
    </header>

    
    <!-- ! header page-->
    <div class="content-body">
      <div class="container page">
        <div class="row">
          <!-- content-->
          <div class="col-md-12 mb-md-70 woocommerce" style="text-align: center;">
            <div><i class="fa fa-times-circle-o fa-5x" style="color:red"></i></div>
			<h3 class="mt-0 mb-30">Ocurrio un error con su pago</h3>    
			<p>No hemos podido cargar el pago de la reserva, para cualquier información puede ponerse en contacto con nosotros, y gustosamente le atenderemos</p>
			<p><a href="<?= base_url('p/contacto') ?>">Solicitud de Contacto</a></p>
            
        </div> 
      </div>
    </div>
</div>
