<header>
      <?php $this->load->view('includes/template/header'); ?>
      <!-- End Navigation panel-->
      <!-- breadcrumbs start-->
      <section class="breadcrumbs style-2 gray-90">
        <div class="container">
          <div class="text-left breadcrumbs-item">
          	<a href="<?= base_url() ?>">Inicio</a><i>/</i>
          	<a href="<?= base_url('experience') ?>">Experience</a><i>/</i>
          	<a href="<?= base_url('checkout') ?>" class="last">
          		<span>Reserva confirmada</span>
          	</a>
            <h2><span>Reserva confirmada</span></h2>
          </div>
        </div>
      </section>
      <!-- ! breadcrumbs end-->
    </header>

    
    <!-- ! header page-->
    <div class="content-body">
      <div class="container page">
        <div class="row">
          <!-- content-->
          <div class="col-md-12 mb-md-70 woocommerce" style="text-align: center;">
            <div><i class="fa fa-check fa-5x" style="color:green"></i></div>
      			<h3 class="mt-0 mb-30">Su reserva se ha confirmado satisfactoriamente</h3>    
      			<p>Su reserva se ha almacenado satisfactoriamente y estará pendiente de procesar el pago, si ha realizado el pago con tarjeta de crédito le enviaremos un correo electrónico cuando el pago de la misma se haya recibido.</p>
            <p>Si ha seleccionado el modo de pago en efectivo recuerda que debes realizar tu pago en nuestras <a href="<?= base_url('p/contacto') ?>">oficinas</a>. </p>
            <p>Para más información puedes consultar nuestra <a href="<?= base_url('p/contacto') ?>">página de contacto</a>, escribirnos en <a href="mailto:info@kanvoy.com">info@kanvoy.com</a> o llamarnos a los números <a href="tel:+34881992303">881 992 303</a> - <a href="tel:+34881992303">881 992 303</a></p>
            
        </div>
      </div>
    </div>
</div>
