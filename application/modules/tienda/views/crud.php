<?= $output ?>
<script>
	var input = document.getElementById('field-direccion'); 
	var autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
	autocomplete.addListener('place_changed', fillInAddress);
	console.log(mapa.marker);
	function fillInAddress(){
	    var place = autocomplete.getPlace().geometry.location;    
	    mapa.marker.setPosition(new google.maps.LatLng(place.lat(),place.lng()));
	    mapa.map.panTo(new google.maps.LatLng(place.lat(),place.lng()));
	    $("#field-mapa").val(mapa.marker.getPosition());
	    //google.maps.event.addListener(mapa.marker,'dragend',function(){$("#field-mapa").val(mapa.marker.getPosition())});
	}
</script>