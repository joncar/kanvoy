<header><?php $this->
load->view('includes/template/header'); ?></header>
<!-- ! header page-->
<div class="content-body">
	<div class="tp-banner-container">
		<div class="tp-banner-slider">
			<ul>
				<?php 
					$this->db->order_by('orden','ASC');
					foreach($this->db->get('slides')->result() as $s): 
				?>
					<li data-masterspeed="700" data-slotamount="7" data-transition="fade" style="margin-left: 0px; padding-left: 0px;" data-mce-style="margin-left: 0px; padding-left: 0px;"><img src="<?= base_url() ?>rs-plugin/assets/loader.gif" data-lazyload="<?= base_url('img/slides/'.$s->foto) ?>" data-bgposition="center" alt="" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10">
						<div data-x="['center','center','center','center']" data-y="center" data-transform_in="x:-150px;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:150px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-start="400" class="tp-caption sl-content">
							<div class="sl-title-top">
								<?= $s->linea1 ?>
							</div>
							<div class="sl-title">
								<?= $s->linea2 ?>
							</div>
							<div class="sl-title-bot">
								<?= $s->linea3 ?>
							</div>
						</div>
					</li>
				<?php endforeach ?>
			</ul>
		</div>
		<!-- slider info-->
		
	</div>
	<?php 
		$experience = $this->db->get('experience_content')->row();
	?>
	<!-- page section-->
	<section class="page-section pb-0">
		<div class="container">
			<div class="row patinete">
				<div class="col-md-8">
					<h6 class="title-section-top font-4"><?= $experience->texto_naranja_actual ?> </h6>
					<h2 class="title-section"><?= $experience->titulo_actual ?></h2>
					<div class="cws_divider mb-25 mt-5">
						<br>
					</div>
					<p>
                        <?= $experience->texto_actual ?>
					</p>
				</div>
				<div class="col-md-4">
					<img src="<?= base_url('img/experience_content/'.$experience->foto_actual) ?>"  alt="" class="mt-md-0">
				</div>
			</div>
		</div>
		<div class="features-tours-full-width">
			<div class="features-tours-wrap clearfix">
				<?php 
					$this->db->order_by('orden','ASC');
					$this->db->where('fecha >',date("Y-m-d"));
					foreach($this->db->get_where('productos',array('proximamente'=>0))->result() as $d): 
				?>
				<div class="features-tours-item">
					<div class="features-media">
						<img style="width: 480px;" src="<?=base_url('img/tienda/'.$d->portada) ?>" alt="" data-mce-style="width: 480px;">
						<div class="features-info-top">
							<div class="info-price font-4">
								<span>Precio x Persona</span><?= moneda($d->precio) ?>€
							</div>
							<div class="info-temp font-4">
								<span>Salidas desde </span>
								<div style="vertical-align: top; display: inline-block;"><?= $d->capacidad ?></div>
							</div>
							<p class="info-text">
								<?= substr(strip_tags($d->descripcion_corta),0,255) ?>
							</p>
						</div>
						<div class="features-info-bot">
							<h4 style="color:white; font-size:16px;"><?= strftime('%d %b %Y',strtotime($d->fecha)) ?></h4>
							<h3 class="title">
								<span class="font-4"><?= $d->nombre ?></span><?= $d->grupo ?>
							</h3>
							<a href="<?=site_url('experience/'.toURL($d->id.'-'.$d->nombre)) ?>" class="button">Detalles</a><br>
						</div>
					</div>

					<div class="contador">
						<div class="flex-w flex-c cd<?= $d->id ?> wsize1">
							
							<div class="flex-col-c-m size2 bor2">
								<span class="l1-txt3 p-b-7 days">35</span>
								<span class="s1-txt1">Dias</span>
							</div>

							<div class="flex-col-c-m size2 bor2">
								<span class="l1-txt3 p-b-7 hours">17</span>
								<span class="s1-txt1">Hrs</span>
							</div>

							<div class="flex-col-c-m size2 bor2">
								<span class="l1-txt3 p-b-7 minutes">50</span>
								<span class="s1-txt1">Min</span>
							</div>

							<div class="flex-col-c-m size2">
								<span class="l1-txt3 p-b-7 seconds">39</span>
								<span class="s1-txt1">Seg</span>
							</div>
							<div class=" size2" style="width: auto;margin: 0 10px; padding-top: 18px;     margin-left: 10px;">
								<span class="l1-txt3 p-b-7"><a href="<?=site_url('experience/'.toURL($d->id.'-'.$d->nombre)) ?>" class="button reservar">Reservar</a></span><br>
							</div>
						</div>
					</div>
				</div>

				<script>
					$(document).on('ready',function(){
						$('.cd<?= $d->id ?>').countdown100({
							// Set Endtime here
							// Endtime must be > current time
							endtimeYear: <?= date("Y",strtotime($d->fecha)) ?>,
							endtimeMonth: <?= date("m",strtotime($d->fecha)) ?>,
							endtimeDate: <?= date("d",strtotime($d->fecha)) ?>,
							endtimeHours: 0,
							endtimeMinutes: 0,
							endtimeSeconds: 0,
							timeZone: "Europe/Spain" 
							// ex:  timeZone: "America/New_York", can be empty
							// go to " http://momentjs.com/timezone/ " to get timezone
						});
					});
				</script>
				
				<?php endforeach ?>
			</div>
		</div>
	</section><section class="page-section pb-0">
		<div class="container">
			<div class="row patinete">
				<div class="col-md-8">
					<h6 class="title-section-top font-4"><?= $experience->texto_naranja_proxima ?> </h6>
					<h2 class="title-section"><?= $experience->titulo_proxima ?></h2>
					<div class="cws_divider mb-25 mt-5">
						<br>
					</div>
					<p>
                        <?= $experience->texto_proxima ?>
					</p>
				</div>
				<div class="col-md-4">
					<img src="<?= base_url('img/experience_content/'.$experience->foto_proxima) ?>"  alt="" class="mt-md-0">
				</div>
			</div>
		</div>
		<div class="features-tours-full-width">
			<div class="features-tours-wrap clearfix">
				<?php 
					$this->db->order_by('orden','ASC');
					foreach($this->db->get_where('productos',array('proximamente'=>1))->result() as $d): ?>
				<div class="features-tours-item">
					<div class="features-media">
						<img style="width: 480px;" src="<?=base_url('img/tienda/'.$d->portada) ?>" alt="" data-mce-style="width: 480px;">
						<div class="features-info-top">
							
								<div class="info-price font-4">
									<?php if($d->precio>0): ?>
									<span>Precio x Persona</span><?= moneda($d->precio) ?>€
									<?php endif;  ?>
								</div>
							
							<div class="info-temp font-4">
								<span>Salidas desde </span>
								<div style="line-height: 23px;vertical-align: top; display: inline-block;"><?= $d->capacidad ?></div>
							</div>
							<p class="info-text">
								<?= substr(strip_tags($d->descripcion_corta),0,255) ?>
							</p>
						</div>
						<div class="features-info-bot">
							<h4 style="color:white; font-size:16px;"><?= strftime('%d %b %Y',strtotime($d->fecha)) ?></h4>
							<h3 class="title">
								<span class="font-4"><?= $d->nombre ?></span><?= $d->grupo ?>
							</h3>
							<a href="<?=site_url('experience/'.toURL($d->id.'-'.$d->nombre)) ?>" class="button">Detalles</a><br>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</section>

<!-- ! gallery section-->

<!-- ! latest news-->
<!-- call out section-->
<?php $this->
load->view('includes/template/subscribe'); ?>
</div>
