<header>
    <?php $this->load->view('includes/template/header'); ?>
    <!-- breadcrumbs start-->
    <section style="background-image:url('<?= base_url() ?>pic/footer/footer-bg.jpg'); background-repeat: no-repeat; background-position: center; background-size: cover" class="breadcrumbs style-2 gray-90">
      <div class="container">
        <div class="text-left breadcrumbs-item">
          	<a href="<?= base_url() ?>">Inicio</a><i>/</i>
          	<a href="<?= base_url('experience') ?>">Experience</a><i>/</i>
          	<a href="<?= base_url('checkout') ?>" class="last">
          		<span>Confirmar compra</span>
          	</a>
            <h2><span>Confirmar compra</span></h2>
          </div>
      </div>
    </section>
</header>   

    
    <!-- ! header page-->
    <div class="content-body">
      <div class="container page">
        <div class="row">
          <!-- content-->
          <div class="col-md-8 mb-md-70 woocommerce">
            <div id="result" class="result"></div>
			<h3 class="mt-0 mb-30">Destinos</h3>    

            <div>
            	<form name="checkout" id="carrito" method="post" action="" class="carrito woocommerce-checkout">
            		<?php $this->load->view('_carrito') ?>
            	</form>
            </div>
            
         
          </div>
          <!-- ! content-->
          <!-- sidebar-->
          <div class="col-md-4 sidebar">
			<div class="orden">
				<?php $this->load->view('_orden'); ?>
			</div>          	

			<form action="" onsubmit="return reservar(this)">
	            <div id="payment" class="woocommerce-checkout-payment">
	              
	              <div class="payment_methods methods">               
	                <div class="payment_method_bacs">
	                  <input id="payment_method_bacs" type="radio" name="tipo_pago" value="Efectivo" class="input-radio">
	                  <label for="payment_method_bacs" style="margin-left:37px">Efectivo</label>
	                  <div class="payment_box payment_method_bacs">
	                    <p>Puedes realizar tu pago en efectivo en nuestras oficinas (Mira las direcciones en el pie de la web).</p>	                    
	                  </div>
	                </div> 

	                <div class="payment_method_paypal">
	                  <input id="payment_method_paypal" name="tipo_pago" value="TPV" class="input-radio" checked="checked"  type="radio">
	                  <label for="payment_method_paypal">Tarjeta de crédito <i class="fa fa-cc-visa fa-2x" aria-hidden="true"></i></label>
	                  <div style="display: none" class="payment_box payment_method_paypal">
	                    <p>Con tarjeta de crédito</p>
	                  </div>
	                </div> 

	                <div class="payment_method_paypal" style="text-align: center; padding-top:10px">
	                  <div style="display:inline-block" class="g-recaptcha" data-sitekey="6LdA1GUUAAAAAEnpl5OAK2xKD7NWOl9tJP_7F02X"></div>
	                </div> 

	              </div>
				  
	              <div class="place-order mt-20">
	              	<div style="margin:8px">
	              		<input name="politicas" value="1" class="cws-checkbox" style="width: 30px;height: 30px;padding: 5px;position: relative;display: inline-block;margin: 0;" type="checkbox"> Acepto las <a href="<?= base_url('p/aviso-legal') ?>" target="_new"><u>políticas de privacidad</u></a>
	              	</div>
	              	<?php if(count($_SESSION['productos'])>0): ?>
	                <input id="place_order" type="submit" name="" value="Pagar" class="cws-button full-width alt">
	            	<?php endif ?>
	              </div>	              
	              <div class="clear"></div>
	              <div id="result" class="result visible-xs"></div>
	            </div>
			</form>
          </div>
          <!-- ! sidebar-->
        </div>
      </div>
    </div>


<?php /*echo '<form id="formPay" action="http://tpv.ceca.es:8000/cgi-bin/tpv" method="post" enctype="application/x-www-form-urlencoded">';*/ ?>
<form id="formPay" action="https://pgw.ceca.es/cgi-bin/tpv" method="post" enctype="application/x-www-form-urlencoded">
    <input name="MerchantID" type=hidden value="081534372">
    <input name="AcquirerBIN" type=hidden value="0000554026">
    <input name="TerminalID" type=hidden value="00000003">
    <input name="URL_OK" type=hidden value="<?= base_url('pagook') ?>">
    <input name="URL_NOK" type=hidden value="<?= base_url('pagonok') ?>">
    <input id="firma" name="Firma" type=hidden value="">
    <input name="Cifrado" type=hidden value="SHA1">
    <input id="operacion" name="Num_operacion" type=hidden value="">
    <input id="importe" name="Importe" type=hidden value="">
    <input name="TipoMoneda" type=hidden value="978">
    <input name="Exponente" type=hidden value="2">
    <input name="Pago_soportado" type=hidden value="SSL">
    <input name="Idioma" type=hidden value="1">
</form>

    <script>
    	$(document).on('change','.qty',function(){
    		actualizar();
    	});

    	$(document).on('click','.remove',function(e){
    		e.preventDefault();
    		$(this).parents('tr').remove();
    		actualizar();
    	});



    	function actualizar(){
			var f = new FormData(document.getElementById('carrito'));
	        $.ajax({
	            url:'<?php echo base_url('tienda/frontend/actualizarCarro') ?>',
	            data: f,
	            context: document.body,
	            cache: false,
	            contentType: false,
	            processData: false,
	            type: 'POST',
	            success:function(data){
	                data = JSON.parse(data);
	             	$(".carrito").html(data.carrito);
	             	$(".orden").html(data.resumen); 
	             	$("#result").html(data.errorMessage);
	            }
	        });
	        return false;
    	}


    	function reservar(form){    		
    		if($("input[name=politicas]").prop('checked')){
		        var f = new FormData(document.getElementById('carrito'));
		        //$("#place_order").attr('disabled',true);
		        var tipo_pago = $("input[name=tipo_pago]:checked").val();
		        f.append('tipo_pago',tipo_pago);	
		        f.append('g-recaptcha-response',$(".g-recaptcha-response").val())        
		        $.ajax({
		            url:'<?php echo base_url('tienda/frontend/reservar') ?>',
		            data: f,
		            context: document.body,
		            cache: false,
		            contentType: false,
		            processData: false,
		            type: 'POST',
		            success:function(data){
		                data = JSON.parse(data);
		                if(data.success){
		                  if(tipo_pago=='TPV'){
			                  $("#importe").val(data.importe);
			                  $("#operacion").val(data.id);
			                  $("#firma").val(data.firma);
			                  $("#formPay").submit();	                  
		              	  }else{
		              	  	document.location.href="<?= base_url('pagook'); ?>";
		              	  }
		                }else{
		                  $(".result").html(data.error_message);
		                }
		            }
		        });
	    	}else{
	    		$("#result").html('<div class="alert alert-danger">Debes aceptar nuestras politicas de privacidad</div>');
	    	}
	        return false;
	    }
    </script>