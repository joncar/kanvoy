<h1>Resumen de venta</h1>

<?php $venta = $this->db->get_where('ventas',array('id'=>$venta)); ?>
<?php if($venta->num_rows()>0): ?>	
	<?php 
		$venta = $venta->row();
		$venta->productos = $this->db->get_where('ventas_detalles',array('ventas_id'=>$venta->id));		
	?>
	<h3>Reserva #<?= $venta->transaccion ?></h3>
	<table style="width:100%;" cellpadding="0" cellspacing="0">
		<thead>
			<tr style="background: #fafafa;">
				<td></td>
				<td style="padding:15px">Destino</td>
				<td style="text-align: right; padding:15px; ">Precio</td>
				<td style="text-align: right; padding:15px;">Cantidad</td>
				<td style="text-align: right; padding:15px;">Total</td>
			</tr>
		</thead>
		<tbody style="border:1px solid #fafafa;">
			<?php foreach($venta->productos->result() as $p): ?>
				<?php 
					$producto = $this->db->get_where('productos',array('id'=>$p->productos_id));
					$producto = $producto->row();
				 ?>
				<tr class="" style="">
			        <td style="text-align:center; padding:15px; border:1px solid #fafafa;">
			        	<a href="<?= base_url() ?>experience/<?= toUrl($producto->id.'-'.$producto->nombre) ?>">
			        		<img src="<?= base_url() ?>img/tienda/<?= $producto->portada ?>" style="width:120px">
			        	</a>
			        </td>
			        <td style="padding:15px; border:1px solid #fafafa;">
			        	<a href="<?= base_url() ?>experience/6-fiestas-del-ap--stol-santiago">

							<div class="item-recent clearfix" style="margin-top:7px">	                      
			                  <p class="font-4" style="margin: 0;padding: 0;font-size: 12px;"><?= $producto->descripcion_corta ?></p>
			                  <h3 class="title" style="margin: 0;font-size: 15px;"><?= $producto->nombre ?> </h3>	                      
			                  <div class="date-recent"><?= strftime('%d %b %Y',strtotime($producto->fecha)) ?></div>
			                </div>
			                <h2><u>Pasajeros</u></h2>
			                <ul>
			                	<?php foreach($this->db->get_where('ventas_pasajeros',array('ventas_detalles_id'=>$p->id))->result() as $e): ?>
			                		<li>
			                			<?= $e->nombre.' '.$e->apellido.' Email: '.$e->email.', Tel: '.$e->telefono.', Edad: '.$e->edad.', Comentario: '.$e->comentario.'<br/> Paradas: '.$e->paradas ?>
			                				
		                			</li>
			                	<?php endforeach ?>
			                </ul>
			    		</a>
			        </td>
			        <td style="padding:15px; text-align: right; border:1px solid #fafafa;">
			        	<?= str_replace('.00','',$p->precio) ?>€
			        </td>
			        <td style="padding:15px; text-align: right; border:1px solid #fafafa;">
			           <?= $p->cantidad ?>
			        </td>
			        <td style="padding:15px; text-align: right; border:1px solid #fafafa;">
			        	<?= str_replace('.00','',$p->total) ?>€
			        </td>
			      </tr>
			<?php endforeach ?>
			<tr class="" style="">
			        <td style="text-align:center; padding:15px; border:1px solid #fafafa;">
			        	
			        </td>
			        <td colspan="3" style="padding:15px; border:1px solid #fafafa;">
			        	<b> Total venta</b>
			        </td>
			        
			        <td style="padding:15px; text-align: right; border:1px solid #fafafa;">
			        	<?= str_replace('.00','',$venta->total) ?>€
			        </td>
			      </tr>
	      </tbody>
	</table>
<?php endif ?>

<table height="100" style="width: 609px;">
<tbody>
<tr>
<td style="width: 221.25px;"><img src="http://www.kanvoy.com//img/uploads/logo.png" alt="" /></td>
<td style="width: 372.75px;">C/ Condes de Andrade N&ordm; 1 L.1<br />Culleredo, A CORU&Ntilde;A. C.P.15174<br /><br />Tel. 881 992 303<br />email:&nbsp;<a href="info@kanvoy.com">info@kanvoy.com</a></td>
</tr>
</tbody>
</table>
<p></p>