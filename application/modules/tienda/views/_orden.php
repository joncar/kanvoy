<div class="col-2">
  <h3 id="order_review_heading" class="mt-0 mb-30">Tu orden</h3>                  
  <div id="order_review" class="woocommerce-checkout-review-order">
    <table class="shop_table woocommerce-checkout-review-order-table">
      <thead>
        <tr>
          <th class="product-name">Destino</th>
          <th class="product-total">Total</th>
        </tr>
      </thead>
      <tbody>
      	<?php $total = 0; ?>
      	<?php foreach($_SESSION['productos'] as $n=>$p): ?>
	        <tr class="cart_item">
	          <td class="product-name">
              <div class="item-recent clearfix" style="margin-top:7px">                       
                <p class="font-4" style="margin: 0;padding: 0;font-size: 12px;"><i class="flaticon-suntour-map"></i> <?= $p->descripcion_corta ?></p>
                <h3 class="title" style="margin: 0;font-size: 15px;"><?php echo $p->nombre ?> </h3>
                <div class="date-recent"><?= strftime('%d %b %Y',strtotime($p->fecha)) ?></div>
              </div>
            </td>
	          <td><span class="amount"><?= str_replace('.00','',$p->precio*$p->cantidad) ?>€</span></td>
	        </tr>
	     <?php $total+=$p->precio*$p->cantidad;  ?>
        <?php endforeach ?>
      </tbody>
      <tfoot>
        
        <tr class="order-total">
          <th>Total Compra</th>
          <th><span class="amount"><?= str_replace('.00','',$total) ?>€</span></th>
        </tr>
      </tfoot>
    </table>
  </div>
</div>