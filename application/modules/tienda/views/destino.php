<header>
    <?php $this->load->view('includes/template/header'); ?>
    <!-- breadcrumbs start-->
    <section style="background-image:url('<?= base_url('img/tienda/'.$detail->banner) ?>'); background-repeat: no-repeat; background-position: center; background-size: cover" class="breadcrumbs style-2 gray-90">
      <div class="container">
        <div class="text-left breadcrumbs-item">
            <a href="<?= base_url() ?>">Inicio</a><i>/</i>
            <a href="<?= base_url('experience') ?>">Experience</a><i>/</i>
            <a href="#" class="last"><?php echo $detail->nombre ?></a>
          <h2 style="margin-top:5px">
              <?php echo $detail->grupo ?> / <?php echo $detail->capacidad ?> 
              <?php if($detail->precio>0): ?>
              <span><?php echo moneda($detail->precio) ?> €</span> 
            <?php endif ?>
          </h2>
          <div class="location">
            <a href="#" style="text-decoration: none; border:0">            
              <p class="font-4">
                <i class="fa fa-calendar"></i> &nbsp; <?= strftime('%d %b %Y',strtotime($detail->fecha)) ?>
              </p>
            </a>
            <i class="flaticon-suntour-map"></i> &nbsp; <p class="font-4"><?php echo $detail->direccion ?></p>            
          </div>
        </div>
      </div>
    </section>
</header>    
    <!-- ! breadcrumbs end-->
<!-- End Navigation panel-->           
    <div class="content-body">
      <section class="page-section pt-0 pb-50">
        <div class="container">
          <div class="menu-widget botonera with-switch mt-30 mb-30">
            <ul class="magic-line">
                <li class="current_item"><a href="#overview" class="scrollto">Descripción</a></li>                
                <li><a href="#location" class="scrollto">Lugar</a></li>
                <li class="external">
                  <a href="<?= base_url('p/contacto') ?>">Contacto</a>                                                  
                </li>                         
                  
                                   
                      
                <li class="iconoDerechas hidden-xs" title="VISA">
                  <a href="" style="padding: 10px 6px;">
                    <i class="fa fa-2x fa-cc-visa"></i>                    
                  </a>
                </li> 
                <li class="iconoDerechas hidden-xs" title="Fotografía">
                  <a href="" style="padding: 10px 6px;">
                    <i class="fa fa-2x fa-camera"></i>
                  </a>
                </li>
                <li class="iconoDerechas hidden-xs" title="Staf">
                  <a href="" style="padding: 10px 6px;">
                    <i class="fa fa-2x fa-users"></i>                    
                  </a>
                </li> 
                <li class="iconoDerechas hidden-xs" title="Transporte">
                  <a href="" style="padding: 10px 6px;">
                    <i class="fa fa-2x fa-bus"></i>                    
                  </a>
                </li> 

                <li class="iconoDerechas visible-xs" title="Transporte">
                  <a href="">
                    <i class="fa fa-2x fa-bus"></i>                    
                  </a>
                </li>
                <li class="iconoDerechas visible-xs" title="Staf">
                  <a href="">
                    <i class="fa fa-2x fa-users"></i>                    
                  </a>
                </li> 
                <li class="iconoDerechas visible-xs" title="Fotografía">
                  <a href="">
                    <i class="fa fa-2x fa-camera"></i>
                  </a>
                </li>
                <li class="iconoDerechas visible-xs" title="VISA">
                  <a href="">
                    <i class="fa fa-2x fa-cc-visa"></i>                    
                  </a>
                </li> 
                
                
                 
                <li id="magic-line" style="width: 122px; left: 23.55px;"></li>
                <li id="magic-line"></li>
            </ul>
          </div>
        </div>
        <div class="container">
          <div id="flex-slider" class="flexslider">
            
          <div class="flex-viewport" style="overflow: hidden; position: relative;"></div>
          <ul class="flex-direction-nav">
              <li class="flex-nav-prev">
                  <a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a>
              </li>
              <li class="flex-nav-next">
                  <a class="flex-next" href="#">Next</a>
              </li>
          </ul>
          <div class="flex-viewport" style="overflow: hidden; position: relative;">
              <ul class="slides" style="width: 2000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                  <?php foreach($fotos->result() as $f): ?>
                    <li class="flex-active-slide" style="width: 1170px; margin-right: 0px; float: left; display: block;">
                        <img src="<?= base_url('img/tienda/'.$f->foto) ?>" alt="" draggable="false">
                    </li>
                  <?php endforeach ?>
              </ul>
          </div>
          <ul class="flex-direction-nav">
              <li class="flex-nav-prev">
                  <a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a>
              </li>
              <li class="flex-nav-next">
                  <a class="flex-next" href="#">Next</a>
              </li>
          </ul>
          </div>
          <div id="flex-carousel" class="flexslider">            
            <div class="flex-viewport" style="overflow: hidden; position: relative;"></div>
            <ul class="flex-direction-nav">
                <li class="flex-nav-prev">
                    <a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li>
            </ul>
            <div class="flex-viewport" style="overflow: hidden; position: relative;">
                <ul class="slides" style="width: 2000%; transition-duration: 0s; transform: translate3d(0px, 0px, 0px);">
                    <?php foreach($fotos->result() as $f): ?>
                        <li class="flex-active-slide" style="width: 162px; height:112px; margin-right: 5px; float: left; display: block;">                            
                            <img src='<?= base_url('img/tienda/'.$f->foto) ?>' style="height:112px; width:auto;">                                
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>
            <ul class="flex-direction-nav">
                <li class="flex-nav-prev">
                    <a class="flex-prev flex-disabled" href="#" tabindex="-1">Previous</a>
                </li>
                <li class="flex-nav-next">
                    <a class="flex-next" href="#">Next</a>
                </li>
            </ul>
          </div>
        </div>
        <div class="container mt-30">          
          <div class="row">
            <div class="col-md-8">
              <div class="row" style="margin-left:0px; margin-right: 0px;">
                <?php echo $detail->descripcion ?>
              </div>
              <div class="row" style="margin-left:0px; margin-right: 0px;">
                <h4 class="trans-uppercase mb-10">Lugar</h4>
                <div class="cws_divider mb-30" id="location"></div>
                <!-- google map-->
                <div class="map-wrapper" id="map"></div>
                <ul class="icon inline mt-20">
                  <li> <a href="#"><?php echo $detail->direccion ?><i class="flaticon-suntour-map"></i></a></li>
                  <li> <a href="#"><?php echo $detail->telefono ?><i class="flaticon-suntour-phone"></i></a></li>
                </ul>
              </div>

            </div>
            
            <div class="col-md-4"  id="prices">
                <?php if($detail->proximamente==0 && $detail->cupos_disponibles > 0): ?>
                  <h4 class="trans-uppercase mb-10">RESERVAR</h4>
                  <div class="cws_divider mb-30"></div>
                    <form name="checkout" method="post" action="" onsubmit="return reservar(this)" class="checkout woocommerce-checkout">
                      <div id="customer_details" class="col2-set">
                        <div class="mb-sm-50">


                          <div class="billing-wrapper">
                            <div class="woocommerce-billing-fields">

                              <p id="billing_first_name_field" class="form-row form-row-first validate-required">
                                <label for="billing_first_name">Nombres <abbr title="required" class="required">*</abbr></label>
                                <input id="billing_first_name" name="nombre" placeholder="" value="<?= @$_SESSION['usuario']['nombre']; ?>" class="input-text" type="text">
                              </p>
                              <p id="billing_last_name_field" class="form-row form-row-last validate-required">
                                <label for="billing_last_name">Apellidos <abbr title="required" class="required">*</abbr></label>
                                <input id="billing_last_name" name="apellido" placeholder="" value="<?= @$_SESSION['usuario']['apellido']; ?>" class="input-text" type="text">
                              </p>
                              <div class="clear"></div>                              
                              <p id="billing_email_field" class="form-row form-row-last">
                                <label for="billing_email">Edad<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_email" name="edad" placeholder="" value="<?= @$_SESSION['usuario']['edad']; ?>" class="input-text" type="text">
                              </p>
                              <p id="billing_email_field" class="form-row form-row-last">
                                <label for="billing_email">Telefono<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_email" name="telefono" placeholder="" value="<?= @$_SESSION['usuario']['telefono']; ?>" class="input-text" type="text">
                              </p>
                              <div class="clear"></div>
                              <p id="billing_postcode_field" class="form-row form-row-wide">
                                <label for="billing_postcode">Email<abbr title="required" class="required">*</abbr></label>
                                <input id="billing_postcode" name="email" placeholder="Email" value="<?= @$_SESSION['usuario']['email']; ?>" class="input-text" type="text">
                              </p>
                            </div>
                            <div class="woocommerce-shipping-fields mt-10">                                                          
                                <label for="order_comments">Comentario</label>
                                <textarea id="order_comments" name="comentario" placeholder="" rows="2" cols="5" class="input-text" style="width: 100%; height: 80px;"><?= @$_SESSION['usuario']['comentario']; ?></textarea>
                            </div>
                            <div class="place-order mt-20">
                              <input type="hidden" name="productos_id" value="<?= $detail->id ?>"> 
                              <div style="margin:8px"><input name="politicas" value="1" class="cws-checkbox" style="width: 20px;height: 20px;padding: 5px;position: relative;display: inline-block;margin: 0;" type="checkbox"> Acepto las <a href="<?= base_url('p/aviso-legal') ?>" target="_new"><u>políticas de privacidad</u></a>                             </div>
                              <input id="place_order" value="Añadir e ir al carro" class="cws-button full-width alt" type="submit">
                            </div>
                            <div id="result"></div>
                          </div>
                        </div>

                      </div>
                  </form>
                <?php elseif($detail->cupos_disponibles==0): ?>
                  <h4 class="trans-uppercase mb-10">RESERVAR</h4>
                  <div class="cws_divider mb-30"></div>
                  <h5 class="trans-uppercase mb-10">Lo sentimos pero las vacantes han sido agotadas.</h5>
                <?php else: ?>
                  <h4 class="trans-uppercase mb-10">Proximamente podrás reservar este viaje</h4>
                <?php endif ?>


                <div class="cws-widget" style="margin-top:60px">
                <div class="widget-post">
                  <h2 class="widget-title alt">Proximamente</h2>
                  <!-- item recent post-->
                  <?php foreach($this->db->get_where('productos',array('proximamente'=>1))->result() as $d): ?>
                    <div class="item-recent clearfix">
                      <div class="widget-post-media"><img src="<?=base_url('img/tienda/'.$d->portada) ?>" alt=""></div>
                      <p class="font-4" style="margin: 0;padding: 0;font-size: 12px;"><i class="flaticon-suntour-map"></i> <?= $d->descripcion_corta ?></p>
                      <h3 class="title" style="margin: 0;font-size: 15px;"><?php echo $d->nombre ?> 
                      <br/><?= $d->precio>0?'<span style="color:#fabe09; font-size:15px">'.moneda($d->precio).'€</span>':'' ?></a></h3>                      
                      <div class="date-recent"><?= strftime('%d %b %Y',strtotime($d->fecha)) ?></div>
                    </div>
                    <!-- ! item recent post-->
                  <?php endforeach ?>
                </div>
              </div>
                        
              </div>
            
          </div>
        </div>
        <!-- section prices-->
        
        
      </section>
    </div>







<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyB_R8j4rq6MXoV17xrbyyh2rwZS6pTMd9w&libraries=drawing,geometry,places"></script> 
<script>
    
    function mapa(){
        //Inicializar mapa    
        var mapaContent = document.getElementById('map');
        var center = new google.maps.LatLng<?php echo $detail->mapa ?>;    
        var mapOptions = {zoom: 12,center: center};
        var map = new google.maps.Map(mapaContent, mapOptions);
        new google.maps.Marker({position: center,map: map});
    }

    function reservar(form){
        var f = new FormData(form);
        //$("#place_order").attr('disabled',true);
        $.ajax({
            url:'<?php echo base_url('tienda/frontend/guardar_datos') ?>',
            data: f,
            context: document.body,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success:function(data){
                data = JSON.parse(data);
                if(data.success){
                  //$("#importe").val(data.importe);
                  //$("#operacion").val(data.id);
                  //$("#firma").val(data.firma);
                  //$("#formPay").submit();
                  document.location.href="<?= base_url('checkout') ?>";
                }else{
                  $("#result").html(data.error_message);
                }
            }
        });
        return false;
    }
    mapa(); 
</script>