<?php if(count($_SESSION['productos']) > 0): ?>

	<table class="shop_table cart">
	    <thead>
	      <tr>
	        <th class="product-thumbnail">Destino</th>
	        <th class="product-name">&nbsp;</th>
	        <th class="product-price" style="text-align:center">Precio</th>
	        <th class="product-quantity" style="text-align: center;">Cantidad</th>
	        <th class="product-subtotal" style="text-align: center;">Total</th>
	        <th class="product-remove" style="text-align: center;">&nbsp;</th>
	      </tr>
	    </thead>
	    <tbody>
	      
	      <?php foreach($_SESSION['productos'] as $n=>$p): ?>
	          <tr class="">
	            <td class="product-thumbnail">
	            	<a href="<?= base_url('experience/'.toUrl($p->id.'-'.$p->nombre)) ?>">
	            		<img src="<?= base_url('img/tienda/'.$p->portada) ?>" class="attachment-shop_thumbnail wp-post-image">
	            	</a>
	            </td>
	            <td class="product-name">
	            	<a href="<?= base_url('experience/'.toUrl($p->id.'-'.$p->nombre)) ?>">

						<div class="item-recent clearfix" style="margin-top:7px">	                      
	                      <p class="font-4" style="margin: 0;padding: 0;font-size: 12px;"><i class="flaticon-suntour-map"></i> <?= $p->descripcion_corta ?></p>
	                      <h3 class="title" style="margin: 0;font-size: 15px;"><?php echo $p->nombre ?> </h3>	                      
	                      <div class="date-recent"><?= strftime('%d %b %Y',strtotime($p->fecha)) ?></div>
	                    </div>
            		</a>
	            </td>
	            <td class="product-price" style="text-align:center">
	            	<span class="amount"><?= str_replace('.00','',$p->precio) ?>€</span>
	            </td>
	            <td class="product-quantity" style="text-align: center;">
	              <div class="quantity buttons_added">
	                <input step="1" min="0" name="cart[<?= $n ?>][cantidad]" value="<?= $p->cantidad ?>" title="Cantidad" class="input-text qty text" type="number">
	                <input type="hidden" name="cart[<?= $n ?>][productos_id]" value="<?= $p->id ?>">
	              </div>
	            </td>
	            <td class="product-subtotal" style="text-align: center;">
	            	<span class="amount"><?= str_replace('.00','',$p->precio*$p->cantidad) ?>€</span>
	            </td>
	            <td class="product-remove" style="text-align: center;">
	            	<a href="#" title="Remove this item" class="remove"></a>
	            </td>
	          </tr>

				<tr class="cart_item">
					<td colspan="6" style="padding: 30px;">
						
						<div class="panel-group" id="accordion<?= $p->id ?>" role="tablist" aria-multiselectable="true">
						  <?php for($i=0;$i<$p->cantidad;$i++): ?>
						  <div class="panel panel-default pasajero<?= $n.$i ?>">
						    <div class="panel-heading" role="tab" id="headingOne">
						      <h4 class="panel-title">
						        <a role="button" data-toggle="collapse" data-parent="#accordion<?= $p->id ?>" href="#collapse<?= $p->id.$i ?>" aria-expanded="true" aria-controls="collapse<?= $p->id.$i ?>">
						          Datos del pasajero #<?= $i+1 ?>
						        </a>
						      </h4>
						    </div>
						    <div id="collapse<?= $p->id.$i ?>" class="panel-collapse collapse <?= $i==0?'in':'' ?>" role="tabpanel" aria-labelledby="heading<?= $p->id.$i ?>">
						      <div class="panel-body">
									<div id="customer_details" class="col2-set">
				                        <div class="mb-sm-50">


				                          <div class="billing-wrapper">
				                            <div class="woocommerce-billing-fields">

				                              <p id="billing_first_name_field" class="form-row form-row-first validate-required">
				                                <label for="billing_first_name">Nombres <abbr title="required" class="required">*</abbr></label>
				                                <input id="billing_first_name" name="cart[<?= $n ?>][pasajeros][<?= $i ?>][nombre]" placeholder="" value="<?= $i==0?@$_SESSION['usuario']['nombre']:'' ?>" class="input-text" type="text">
				                              </p>
				                              <p id="billing_last_name_field" class="form-row form-row-last validate-required">
				                                <label for="billing_last_name">Apellidos <abbr title="required" class="required">*</abbr></label>
				                                <input id="billing_last_name" name="cart[<?= $n ?>][pasajeros][<?= $i ?>][apellido]" placeholder="" value="<?= $i==0?@$_SESSION['usuario']['apellido']:'' ?>" class="input-text" type="text">
				                              </p>
				                              <div class="clear"></div>                              
				                              <p id="billing_email_field" class="form-row form-row-last">
				                                <label for="billing_email">Edad<abbr title="required" class="required">*</abbr></label>
				                                <input id="billing_email" name="cart[<?= $n ?>][pasajeros][<?= $i ?>][edad]" placeholder="" value="<?= $i==0?@$_SESSION['usuario']['edad']:'' ?>" class="input-text" type="text">
				                              </p>
				                              <p id="billing_email_field" class="form-row form-row-last">
				                                <label for="billing_email">Telefono<abbr title="required" class="required">*</abbr></label>
				                                <input id="billing_email" name="cart[<?= $n ?>][pasajeros][<?= $i ?>][telefono]" placeholder="" value="<?= $i==0?@$_SESSION['usuario']['telefono']:'' ?>" class="input-text" type="text">
				                              </p>
				                              <div class="clear"></div>
				                              <p id="billing_postcode_field" class="form-row form-row-first">
				                                <label for="billing_postcode">Email<abbr title="required" class="required">*</abbr></label>
				                                <input id="billing_postcode" name="cart[<?= $n ?>][pasajeros][<?= $i ?>][email]" placeholder="Email" value="<?= $i==0?@$_SESSION['usuario']['email']:'' ?>" class="input-text" type="text">
				                              </p>
				                              <p id="billing_postcode_field" class="form-row form-row-last">
				                                <label for="billing_postcode">Paradas<abbr title="required" class="required">*</abbr></label><br/>
				                                <?php foreach(explode(',',$p->paradas) as $parada): ?>
				                                	<input id="billing_postcode" name="cart[<?= $n ?>][pasajeros][<?= $i ?>][paradas][]" value="<?= ucwords(trim($parada)); ?>" class="input-text" type="radio" style="width:12px"> <?= ucwords(trim($parada)); ?><br/>
				                            	<?php endforeach ?>
				                                
				                              </p>
				                            </div>
				                            <div class="woocommerce-shipping-fields mt-10">                                                          
				                                <label for="order_comments">Comentario</label>
				                                <textarea id="order_comments" name="cart[<?= $n ?>][pasajeros][<?= $i ?>][comentario]" placeholder="" rows="2" cols="5" class="input-text" style="width: 100%; height: 80px;"><?= $i==0?@$_SESSION['usuario']['comentario']:'' ?></textarea>
				                            </div>
				                          </div>
			                        	</div>
						      	</div>
						    </div>
						  </div>
						</div>
						  <?php endfor ?>
						</div>


					</td>
				</tr>
	      <?php endforeach ?>
	    </tbody>
	  </table>
	  <a href="<?= base_url('experience') ?>">Añadir Destino</a>

<?php else: ?>

	Carrito vacio, desea ir a los <a href="<?= base_url('experience') ?>" style="text-decoration:underline;">destinos disponibles?</a>
<?php endif ?>