<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function productos($action = '',$id = ''){
            $crud = $this->crud_function('','');            
            $crud->field_type('portada','image',array('path'=>'img/tienda','width'=>'416px','height'=>'276px'))
                 ->field_type('banner','image',array('path'=>'img/tienda','width'=>'1600px','height'=>'200px'))
                 ->display_as('capacidad','Población de salida')
                 ->display_as('nombre','Texto encima del titulo')
                 ->display_as('grupo','Nombre de la fiesta')
                 ->display_as('descripcion_corta','Población');            
            $crud->field_type('mapa','map',array());
            $crud->field_type('actividades','tags')
                 ->field_type('paradas','tags')
                 ->columns('portada','nombre','grupo','precio','fecha','orden','ventas');       
             $crud->order_by('id','DESC');
            $crud->add_action('<i class="fa fa-image"></i> Galeria de fotos','',base_url('tienda/admin/productos_fotos').'/')
                 ->add_action('Descargar pasajeros','',base_url('experiencia/descargar').'/')
                 ->callback_column('ventas',function($val,$row){
                    return (string)$this->db->get_where('ventas_detalles',array('productos_id'=>$row->id))->num_rows();
                 });
            $crud->set_clone();
            $crud = $crud->render();
            $crud->output = $this->load->view('crud',array('output'=>$crud->output),TRUE);
            $this->loadView($crud);
        }  

         function slides($action = '',$id = ''){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','img/slides');                                    
            $crud = $crud->render();            
            $this->loadView($crud);
        } 

        function content($action = '',$id = ''){
            $this->as['content'] = 'experience_content';
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto_actual','img/experience_content');                                    
            $crud->set_field_upload('foto_proxima','img/experience_content');                                    
            $crud->unset_add()
                 ->unset_delete();
            $crud = $crud->render();            
            $this->loadView($crud);
        }   

        function productos_fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('productos_fotos')
                 ->set_url_field('foto')
                 ->set_relation_field('productos_id')
                 ->set_image_path('img/tienda')
                 ->set_ordering_field('orden')
                 ->module = 'tienda';
            $crud = $crud->render();
            $crud->output.= '<p><b>Nota: </b> El tamaño recomendado para las fotos es de 1170x480</p>';
            $crud->title = 'Galería Fotográfica de productos';
            $this->loadView($crud);
        }

        function ventas($x = ''){
            if(empty($x) || $x=='ajax_list' || $x=='ajax_list_info' || $x=='success'){
                $this->as['ventas'] = 'view_ventas';
            }
            $crud = $this->crud_function('','');
            if($crud->getParameters()=='list'){
                $crud->set_primary_key('id');
            }            
            if($crud->getParameters()=='list'){
                $crud->field_type('status','dropdown',array('1'=>'<span class="label label-info">Solicitada</span>','2'=>'<span class="label label-success">Pagada</span>','3'=>'Rechazada'));
            }else{
                $crud->field_type('status','dropdown',array('1'=>'Solicitada','2'=>'Pagada','3'=>'Rechazada'));
            }
            $crud->add_action('Detalle de venta','',base_url('experiencia/venta_detalles').'/');
            $crud = $crud->render();
            $this->loadView($crud);
        }  

        function venta_detalles($venta){
            if(is_numeric($venta)){
                $this->as['venta_detalles'] = 'ventas_detalles';
                $crud = $this->crud_function('','');                
                $crud->where('ventas_id',$venta)
                     ->field_type('ventas_id','hidden',$venta)
                     ->unset_columns('ventas_id')
                     ->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();
                $crud->add_action('Pasajeros','',base_url('experiencia/ventas_pasajeros').'/');
                $crud = $crud->render();
                $this->loadView($crud);
            }else{
                redirect('experiencia/ventas');
            }
        }

        function ventas_pasajeros($venta){
            if(is_numeric($venta)){                
                $crud = $this->crud_function('','');                
                $crud->where('ventas_detalles_id',$venta)
                     ->field_type('ventas_detalles_id','hidden',$venta)
                     ->unset_columns('ventas_detalles_id')
                     ->unset_add()
                     ->unset_edit()
                     ->unset_delete()
                     ->unset_print()
                     ->unset_export()
                     ->unset_read();                
                $crud = $crud->render();
                $this->loadView($crud);
            }else{
                redirect('experiencia/ventas');
            }
        }

        function descargar($producto_id){            
            $query = "SELECT 
                ventas.transaccion,
                productos.nombre,
                ventas_pasajeros.nombre,
                ventas_pasajeros.apellido,
                ventas_pasajeros.edad,
                ventas_pasajeros.email,
                ventas_pasajeros.telefono,
                ventas_pasajeros.comentario,
                ventas_pasajeros.paradas
                FROM ventas_detalles
                INNER JOIN ventas ON ventas.id = ventas_detalles.ventas_id
                INNER JOIN productos ON productos.id = ventas_detalles.productos_id
                INNER JOIN ventas_pasajeros ON ventas_pasajeros.ventas_detalles_id = ventas_detalles.id
                WHERE productos_id = ".$producto_id;
            $paradas = $this->db->query($query.' GROUP BY ventas_pasajeros.paradas');
            if($paradas->num_rows()>0){
                $string_to_export = '';
                foreach($paradas->result() as $p){

                    $string_to_export.= 'PARADA: '.$p->paradas."\n";

                    $pasajeros = $this->db->query($query.' AND ventas_pasajeros.paradas = \''.$p->paradas.'\' ORDER BY apellido ASC');

                    foreach($pasajeros->row() as $n=>$column){
                        $string_to_export .= $n."\t";
                    }
                    $string_to_export .= "\n";
                            
                    foreach($pasajeros->result() as $num_row => $row){
                        foreach($row as $column){
                            $string_to_export .= $column."\t";
                        }
                        $string_to_export .= "\n";
                    }
                    $string_to_export .= " \n";
                }

                $string_to_export = "\xFF\xFE" .mb_convert_encoding($string_to_export, 'UTF-16LE', 'UTF-8');
                $filename = "export-".date("Y-m-d_H:i:s").".xls";
                header('Content-type: application/vnd.ms-excel;charset=UTF-16LE');
                header('Content-Disposition: attachment; filename='.$filename);
                header("Cache-Control: no-cache");
                echo $string_to_export;
                die();
            }
        }
    }
?>
