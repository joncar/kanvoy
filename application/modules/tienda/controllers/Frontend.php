<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
            $this->module = 'Tienda';
        }
        
        function lista($action = '',$id = ''){
            $this->loadView(array('view'=>'tienda'));
        } 

        function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $destinos = new Bdsource();                
                $destinos->where('id',$id);                
                $destinos->init('productos',TRUE);
                
                $recomendados = new Bdsource();
                $recomendados->limit = array(3);                
                $recomendados->where('proximamente',1);                                
                $recomendados->init('productos',FALSE,'recomendados');
                foreach($this->recomendados->result() as $n=>$b){
                    $this->recomendados->row($n)->link = site_url('experience/'.toURL($b->id.'-'.$b->nombre));
                    $this->recomendados->row($n)->foto = base_url('img/tienda/'.$b->portada);
                }
                                
                $fotos = new Bdsource();
                $fotos->where('productos_id',$this->productos->id);
                $fotos->init('productos_fotos');
                $this->loadView(
                    array(
                        'view'=>'destino',
                        'detail'=>$this->productos,
                        'title'=>$this->productos->nombre,
                        'fotos'=>$this->productos_fotos,
                        'recomendados'=>$this->recomendados,
                        'module'=>$this->module,
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }

        function actualizarCarro(){
            $_SESSION['productos'] = array();
            $errorMessage = '';
            foreach($_POST['cart'] as $cart){
                $producto = $producto = $this->db->get_where('productos',array('id'=>$cart['productos_id']));
                if($producto->num_rows()>0){                    
                    $_POST['importe'] = $producto->row()->precio;                                        
                    if($producto->row()->cupos_disponibles<$cart['cantidad']){
                        $errorMessage = 'Lo sentimos pero has excedido el numero máximo permitido de vacantes, elije un numero menor a '.$producto->row()->cupos_disponibles.' en el destino '.$producto->row()->nombre;
                        $cart['cantidad'] = $producto->row()->cupos_disponibles;
                    }
                    $producto->row(0)->cantidad = $cart['cantidad'];
                    $_SESSION['productos'][] = $producto->row();
                }
            }
            $errorMessage = !empty($errorMessage)?$this->error($errorMessage):'';
            echo json_encode(array(
                'carrito'=>$this->load->view('_carrito',array(),TRUE),
                'resumen'=>$this->load->view('_orden',array(),TRUE),
                'errorMessage'=>$errorMessage
            ));
        }

        function guardar_datos(){
            $response = array('success'=>false);
            $this->load->library('form_validation');
            $this->form_validation->set_rules('nombre','Nombre','required')
                                  ->set_rules('apellido','Apellido','required')                                  
                                  ->set_rules('email','Email','required|valid_email')                                  
                                  ->set_rules('productos_id','Producto','required');
            if($this->form_validation->run()){                  
                $producto = $this->db->get_where('productos',array('id'=>$_POST['productos_id']));
                if($producto->num_rows()>0){
                    $response['success'] = true;
                    $_POST['importe'] = $producto->row()->precio;
                    $_SESSION['usuario'] = $_POST;
                    if(empty($_SESSION['productos'])){
                        $_SESSION['productos'] = array();
                    }
                    $enc = false;
                    foreach($_SESSION['productos'] as $p){
                        if($p->id == $producto->row()->id){
                            $enc = true;
                        }
                    }
                    if(!$enc){
                        $producto->row(0)->cantidad = 1;
                        $_SESSION['productos'][] = $producto->row();
                    }

                }else{
                    $response['error_message'] = $this->error('Ha ocurrido un error por favor intente mas tarde <script>$("#place_order").attr("disabled",false); </script>');
                }
            }else{

                $response['error_message'] = $this->error('Por favor complete todos los datos solicitados <script>$("#place_order").attr("disabled",false); </script>');
            }
            echo json_encode($response);
        }

        function validateFields(){
            $result = array('success'=>true);
            $errorMessage = '';    
            $this->load->library('recaptcha');        
            if(!isset($_POST) || empty($_POST['cart'])){
                $errorMessage.= '<div>Por favor complete los datos personales requeridos</div>';            
            }else{
                foreach($_POST['cart'] as $cart){
                    if($cart['cantidad'] != count($cart['pasajeros'])){
                        $errorMessage.= '<div>Por favor complete la información de todos los pasajeros</div>';     
                    }
                    else{
                        foreach($cart['pasajeros'] as $n=>$p){
                            foreach($p as $nn=>$v){
                                if($nn!='comentario' && empty($v)){
                                    $errorMessage.= '<div>En el pasajero '.($n+1).' falta el dato '.$nn.'</div>';     
                                }                                
                            }
                            if(empty($p['paradas'])){
                                $errorMessage.= '<div>En el pasajero '.($n+1).' Elija al menos una parada</div>';        
                            }
                        }
                    }
                }
            }    

            if(empty($_POST['g-recaptcha-response']) || !$this->recaptcha->recaptcha_check_answer($_POST['g-recaptcha-response'])){
                    $errorMessage .= '<div>Captcha introducido incorrectamente</div>';
            }

            if(!empty($errorMessage)){
                $result = array('success'=>false,'error_message'=>$this->error($errorMessage));
                echo json_encode($result);
                die();
            }        
            
        }

        function reservar(){

            $this->validateFields();
            $response = array('success'=>'true');
            if(isset($_POST) && !empty($_POST['cart'])){
                

                $this->db->insert('ventas',array(
                    'status'=>1,
                    'tipo_pago'=>$_POST['tipo_pago']
                ));

                $total = 0;
                $id = $this->db->insert_id();
                $idrand = 'K-'.date("s").$id;
                $email = '';
                foreach($_POST['cart'] as $cart){
                    $producto = $this->db->get_where('productos',array('id'=>$cart['productos_id']));
                    if($producto->num_rows()>0){
                        $producto = $producto->row();
                        $total+= $cart['cantidad']*$producto->precio;
                        $this->db->insert('ventas_detalles',array(
                            'ventas_id'=>$id,
                            'productos_id'=>$producto->id,
                            'cantidad'=>$cart['cantidad'],
                            'precio'=>$producto->precio,
                            'total'=>$cart['cantidad']*$producto->precio
                        ));
                        $detalle = $this->db->insert_id();
                        $cupos = $producto->cupos_disponibles-$cart['cantidad'];
                        $this->db->update('productos',array('cupos_disponibles'=>$cupos),array('id'=>$cart['productos_id']));
                        foreach($cart['pasajeros'] as $p){
                            if($email==''){
                                $email = $p['email'];
                            }
                            $paradas = implode(',',$p['paradas']);
                            $this->db->insert('ventas_pasajeros',array(
                                'ventas_id'=>$id,
                                'ventas_detalles_id'=>$detalle,
                                'nombre'=>$p['nombre'],
                                'apellido'=>$p['apellido'],
                                'email'=>$p['email'],
                                'telefono'=>$p['telefono'],
                                'edad'=>$p['edad'],
                                'comentario'=>$p['comentario'],
                                'paradas'=>$paradas
                            ));
                        }
                    }
                }

                $this->db->update('ventas',array('total'=>$total,'transaccion'=>$idrand,'fecha'=>date("Y-m-d H:i:s")),array('id'=>$id));

                $importe = (int)($total*100);
                $urlok = base_url('pagook');
                $urlnook = base_url('pagonok');
                $entidad  = "0000554026";
                $comercio = "081534372";
                $terminal = "00000003";
                //$clave = "5AU6I1DU"; //Test
                /*******
                    5540500001000004  Caducidad:  AAAA12 (Diciembre del año en curso)  CVV2: 989 
                    5020470001370055  Caducidad:  AAAA12 (Diciembre del año en curso)  CVV2: 989 
                    5020080001000006  Caducidad:  AAAA12 (Diciembre del año en curso)  CVV2: 989 
                    4507670001000009  Caducidad:  AAAA12 (Diciembre del año en curso)  CVV2: 989
                ********/
                $clave = "TA9TS91E"; //Produccion
                $moneda = "978";
                $exponente = "2";            
                $code = $clave.$comercio.$entidad.$terminal.$idrand.$importe.$moneda.$exponente."SHA1".$urlok.$urlnook;
                $signature = sha1($code);
                $response['code']=$code;
                $response['importe'] = $importe;
                $response['id'] = $idrand;
                $response['firma'] = $signature;


                if($_POST['tipo_pago']=='Efectivo'){
                    //Mandamos el email
                    $msj = '<h3 class="mt-0 mb-30">Su reserva se ha confirmado satisfactoriamente</h3>    
                        <p>Su reserva se ha almacenado satisfactoriamente y estará pendiente de procesar el pago</p>
                        <p>recuerda que debes realizar tu pago en nuestras <a href="'.base_url('p/contacto') .'">oficinas</a>. </p>
                        <p>Para más información puedes consultar nuestra <a href="'.base_url('p/contacto') .'">página de contacto</a>, escribirnos en <a href="mailto:info@kanvoy.com">info@kanvoy.com</a> o llamarnos a los números <a href="tel:+34881992303">881 992 303</a> - <a href="tel:+34881992303">881 992 303</a></p>
                        <p><b>El numero de tu reserva es: '.$idrand.'</b></p>'; 
                    $msj = $msj.$this->load->view('_email_datos',array('venta'=>$id),TRUE);                       
                    correo($email,'Reserva realizada cón éxito',$msj);
                    correo('info@kanvoy.com','Reserva realizada en efectivo',$msj);
                }


                unset($_SESSION['productos']);
                echo json_encode($response);
            }
        }

        function testMail(){
            $this->load->view('_email_datos',array('venta'=>4));
        }

        function pagook(){
            $this->loadView('pagook');
        }

        function pagonok(){
            $this->loadView('pagonok');
        }


        function checkout(){
            if(empty($_SESSION['productos'])){
                redirect('experience');
            }
            $this->loadView(array('view'=>'checkout','module'=>$this->module));
        }

        function procesar_pago(){ 

            /*$_POST = array (
                'MerchantID' => '081534372', 
                'AcquirerBIN' => '0000554026', 
                'TerminalID' => '00000003' ,
                'Num_operacion' => 'K-57204' ,
                'Importe' => '000000001400' ,
                'TipoMoneda' => '978 ',
                'Exponente' => '2' ,
                'Referencia' => '12005696551807231002416007000' ,
                'Firma' => 'b28be7aaf58310caba452f7cf738fa377e30a30c', 
                'Num_aut' => '101000' ,
                'BIN' => '554050' ,
                'FinalPAN' => '0004', 
                'Cambio_moneda' => '1,00 ',
                'Idioma' => 1 ,
                'Descripcion' => '',
                'Pais' => 724 ,
                'Tipo_tarjeta' => 'C' ,
                'Codigo_pedido' => '',
                'Codigo_cliente' => '',
                'Codigo_comercio' => '',
                'Caducidad' => '201812' ,
                'Idusuario' => ''
            );*/

            if (!empty($_POST)){            
                $this->db->update('ventas',array('status'=>2),array('transaccion'=>$_POST['Num_operacion']));                                
                $cliente = $this->db->get_where('ventas',array('transaccion'=>$_POST['Num_operacion']));
                if($cliente->num_rows()>0){
                    $this->db->order_by('ventas_detalles_id','ASC');
                    $id = $cliente->row()->id;
                    $cliente = $this->db->get_where('ventas_pasajeros',array('ventas_id'=>$id));
                }
                $var = '<h1>Pago Recibido por '.$cliente->row()->nombre.'</h1>';
                $var.= '<h2>Datos del cliente</h2>';
                foreach($cliente->row() as $n=>$v){
                     $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';
                }
                $var.= '<h2>Datos del pago</h2>';
                foreach($_POST as $n=>$v){
                    if(!is_array($v)){
                        if($n=='Importe'){
                            $v = (int)$v;
                            $v/=100;
                            $v.= '€';
                        }
                        $var.= '<p><b>'.$n.'</b>: '.$v.'</p>';
                    }
                }
                $email = $this->load->view('_email_datos',array('venta'=>$id),TRUE);
                $var = $email.$var;
                //Envio al vendedor                
                correo('info@kanvoy.com','Pago Recibido',$var);
                //Envio al comprador
                correo($cliente->row()->email,'Gracias por su pago','Hola '.$cliente->row()->nombre.' Gracias por tu pago, cada vez estás más cerca de tu gran viaje. Si tienes cualquier consulta no dudes en ponerte en contacto con info@kanvoy.com. '.$email);
            }
        }
        
    }
?>
