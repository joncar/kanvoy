<header><?php $this->load->view('includes/template/header'); ?> <!-- breadcrumbs start-->
  <section style="background-image: url('<?= base_url() ?>pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;" class="breadcrumbs" data-mce-style="background-image: url('<?= base_url() ?>pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;">
    <div class="container">
      <div class="text-left breadcrumbs-item">
        <a href="#">Inicio</a><i>/</i> 
        <a href="#" class="last"><span>Contacto</span></a>
        <h2><span>CONT</span>ACTO</h2>
      </div>
    </div>
  </section><!-- ! breadcrumbs end-->
</header>
<div class="content-body">


  <div class="container page">
    <div class="row">
      <div class="col-md-6">
        <div class="contact-item">
          <h4 class="title-section mt-30">
            <span class="font-bold">Datos de contacto<br></span>
          </h4>
        <div class="cws_divider mb-25 mt-5">
          <br>
        </div>
        <ul class="icon">
          <li><a href="#">info@kanvoy.com<i class="flaticon-suntour-email"></i></a></li>
          <li><a href="#">881 992 303<br><i class="flaticon-suntour-phone"></i></a></li>
          <li>881 992 303</li>
          <li><a href="#">C/ Condes de Andrade, 1 L.1 Culleredo, A Coruña C.P. 15174<i class="flaticon-suntour-map"></i></a></li>
        </ul>
        <p class="mt-20"><strong>Horario de Atención al Cliente</strong></p>
        <p class="mt-20">LUNES A VIERNES DE 10:00 A 13:30H.&nbsp;Y DE 17:00 A 20:00H.</p>
        <div class="contact-cws-social">
          <a href="https://www.twitter.com/_kanvoy" class="cws-social fa fa-twitter" target="_blank"></a>
          <a href="https://www.facebook.com/kanvoy1" class="cws-social fa fa-facebook" target="_blank"></a>
          <a href="https://www.instagram.com/_kanvoy" class="cws-social fa fa-instagram" target="_blank"></a>
          <a href="https://es.linkedin.com/jobs/kanvoy-empleos" class="cws-social fa fa-linkedin" target="_blank"></a><br>
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="map-wrapper">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d376.20341786531!2d-8.383693725501134!3d43.31607042574257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2e64bb7d3c464f%3A0x9e0757e6907589fc!2s%C2%B7Kanvoy+Travel+%26+Enjoy!5e0!3m2!1ses-419!2ses!4v1532338903085" width="600" height="450" frameborder="0" style="border: 0;" allowfullscreen="" data-mce-src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d376.20341786531!2d-8.383693725501134!3d43.31607042574257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd2e64bb7d3c464f%3A0x9e0757e6907589fc!2s%C2%B7Kanvoy+Travel+%26+Enjoy!5e0!3m2!1ses-419!2ses!4v1532338903085" data-mce-style="border: 0;"></iframe>
      </div>
    </div>
  </div>
</div>


<div class="element-section pattern bg-gray-3 relative pt-60 pb-100">
  <div class="container">
    <h4 class="title-section mb-20">
      <span class="font-bold">Contacta con nosotros</span>
    </h4>
    <div id="result"><br>
    </div>
    <div class="widget-contact-form pb-0"><!-- contact-form-->

      <form action="paginas/frontend/contacto" method="post" onsubmit="sendForm(this,'.result');return false;">
        <div class="email_server_responce"><br></div>
        <div class="row">
          <div class="col-md-6 clearfix">
          <div class="input-container">
            <input name="nombre" value="" size="40" placeholder="Nombre" aria-invalid="false" aria-required="true" class="form-row form-row-first" type="text">
          </div>
        </div>
        <div class="col-md-6">
          <div class="input-container">
            <input name="email" value="" size="40" placeholder="Email" aria-required="true" class="form-row form-row-last" type="text">
          </div>
        </div>
      </div>
      <div class="input-container">
        <textarea name="comentario" cols="40" rows="4" placeholder="Comentario" aria-invalid="false" aria-required="true">
          
        </textarea>
      </div>
      <div style="text-align: right;" data-mce-style="text-align: right;">
        <div style="display: inline-block;" class="g-recaptcha" data-sitekey="6LdA1GUUAAAAAEnpl5OAK2xKD7NWOl9tJP_7F02X"></div>
      </div>
      <div style="text-align: right;" data-mce-style="text-align: right;">
        <input name="politicas" value="1" class="cws-checkbox" style="width: 20px; height: 20px; padding: 5px; position: relative; display: inline-block; margin: 0;" type="checkbox" data-mce-style="width: 20px; height: 20px; padding: 5px; position: relative; display: inline-block; margin: 0;">
          Acepto las <a href="<?= base_url('p/aviso-legal') ?>" target="_new">
        <u>políticas de privacidad</u></a>
      </div>
      <div class="result"></div>
        <input id="guardar" value="Contactar" class="cws-button alt" type="submit">
         <!-- /contact-form-->
      </form>

    </div>
   </div>
 </div>
</div>

<script src="<?= base_url('js/frame.js') ?>"></script>
<script>
function contacto(form){
    var f = new FormData(form);
    //$("#guardar").attr('disabled',true);
    $.ajax({
        url:'<?php echo base_url('paginas/frontend/contacto') ?>',
        data: f,
        context: document.body,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success:function(data){
            $("#result").html(data);
        }
    });
    return false;
}
</script>