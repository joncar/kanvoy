<header><?php $this->
load->view('includes/template/header'); ?></header>
<!-- ! header page-->
<div class="content-body">
	<div class="tp-banner-container">
		<div class="tp-banner-slider">
			<ul>
				<li data-masterspeed="700" data-slotamount="7" data-transition="fade" style="margin-left: 0px; padding-left: 0px;" data-mce-style="margin-left: 0px; padding-left: 0px;"><img src="<?= base_url() ?>rs-plugin/assets/loader.gif" data-lazyload="<?= base_url() ?>pic/slider/main/slide-1.jpg" data-bgposition="center" alt="" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10">
					<div data-x="['center','center','center','center']" data-y="center" data-transform_in="x:-150px;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:150px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-start="400" class="tp-caption sl-content">
						<div class="sl-title-top">
							Bienvenido a
						</div>
						<div class="sl-title">
							Kanvoy
						</div>
						<div class="sl-title-bot">
							Viajes <span>con</span> estilo propio
						</div>
					</div>
				</li>
				<li data-masterspeed="700" data-transition="fade" style="margin-left: 0px; padding-left: 0px;" data-mce-style="margin-left: 0px; padding-left: 0px;"><img src="<?= base_url() ?>rs-plugin/assets/loader.gif" data-lazyload="<?= base_url() ?>pic/slider/main/slide-2.jpg" data-bgposition="center" alt="" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10">
					<div data-x="['center','center','center','center']" data-y="center" data-transform_in="y:-150px;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:150px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-start="400" class="tp-caption sl-content">
						<div class="sl-title-top">
							Disfruta con
						</div>
						<div class="sl-title">
							Kanvoy
						</div>
						<div class="sl-title-bot">
							Viajes <span>para no</span> olvidar
						</div>
					</div>
				</li>
				<li data-masterspeed="700" data-transition="fade" style="margin-left: 0px; padding-left: 0px;" data-mce-style="margin-left: 0px; padding-left: 0px;"><img src="<?= base_url() ?>rs-plugin/assets/loader.gif" data-lazyload="<?= base_url() ?>pic/slider/main/slide-3.jpg" data-bgposition="center" alt="" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10">
					<div data-x="['center','center','center','center']" data-y="center" data-transform_in="x:150px;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="x:-150px;opacity:0;s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-start="400" class="tp-caption sl-content">
						<div class="sl-title-top">
							Alucina con
						</div>
						<div class="sl-title">
							Kanvoy
						</div>
						<div class="sl-title-bot">
							Viajes <span>vividos</span> con intensidad
						</div>
					</div>
				</li>
			</ul>
		</div>
		<!-- slider info-->
		<div class="slider-info-wrap clearfix">
			<div class="slider-info-content">
				<div class="slider-info-item">
					<div class="info-item-media">
						<img src="<?= base_url() ?>pic/slider-info-1.jpg" data-at2x="<?= base_url() ?>pic/slider-info-1@2x.jpg" alt="">
						<div class="info-item-text">
							<div class="info-price font-4">
								<span>desde</span>100€
							</div>
							<div class="info-temp font-4">
								<span>Destinos</span>10
							</div>
							<p class="info-text">
								Nunc hendrerit nulla molestie ipsum tincidunt vestibulum. Nunc condimentum nibh.
							</p>
						</div>
					</div>
					<div class="info-item-content">
						<div class="main-title">
							<h3 class="title"><span class="font-4">PARA LOS MÁS PEQUEÑOS&nbsp;</span>GRUPOS PRIMARIA</h3>
							<div class="price">
								desde <span>100€</span>
							</div>
							<a href="hotels-details.html" class="button">Detalles</a><br>
						</div>
					</div>
				</div>
				<div class="slider-info-item">
					<div class="info-item-media">
						<img src="<?= base_url() ?>pic/slider-info-2.jpg" data-at2x="<?= base_url() ?>pic/slider-info-2@2x.jpg" alt="">
						<div class="info-item-text">
							<div class="info-price font-4">
								<span>desde</span>100€
							</div>
							<div class="info-temp font-4">
								<span>Destinos</span>10
							</div>
							<p class="info-text">
								Nunc hendrerit nulla molestie ipsum tincidunt vestibulum. Nunc condimentum nibh.
							</p>
						</div>
					</div>
					<div class="info-item-content">
						<div class="main-title">
							<h3 class="title"><span class="font-4">PARA LOS grandes&nbsp;</span>GRUPOS eso</h3>
							<div class="price">
								desde <span>100€</span>
							</div>
							<a href="hotels-details.html" class="button">Detalles</a><br>
						</div>
					</div>
				</div>
				<div class="slider-info-item">
					<div class="info-item-media">
						<img src="<?= base_url() ?>pic/slider-info-3.jpg" data-at2x="<?= base_url() ?>pic/slider-info-3@2x.jpg" alt="">
						<div class="info-item-text">
							<div class="info-price font-4">
								<span>desde</span>100€
							</div>
							<div class="info-temp font-4">
								<span>Destinos</span>10
							</div>
							<p class="info-text">
								Nunc hendrerit nulla molestie ipsum tincidunt vestibulum. Nunc condimentum nibh.
							</p>
						</div>
					</div>
					<div class="info-item-content">
						<div class="main-title">
							<h3 class="title"><span class="font-4">PARA LOS INDOMABLES</span>GRUPOS BUP</h3>
							<div class="price">
								desde <span>100€</span>
							</div>
							<a href="hotels-details.html" class="button">Detalles</a><br>
						</div>
					</div>
				</div>
				<div class="slider-info-item">
					<div class="info-item-media">
						<img src="<?= base_url() ?>pic/slider-info-4.jpg" data-at2x="<?= base_url() ?>pic/slider-info-4@2x.jpg" alt="">
						<div class="info-item-text">
							<div class="info-price font-4">
								<span>desde</span>100€
							</div>
							<div class="info-temp font-4">
								<span>Destinos</span>10
							</div>
							<p class="info-text">
								Nunc hendrerit nulla molestie ipsum tincidunt vestibulum. Nunc condimentum nibh.
							</p>
						</div>
					</div>
					<div class="info-item-content">
						<div class="main-title">
							<h3 class="title"><span class="font-4">PARA LOS EXPERIMENTADOS</span>GRUPOS UNIVERSITARIOS</h3>
							<div class="price">
								desde <span>100€</span>
							</div>
							<a href="hotels-details.html" class="button">Detalles</a><br>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- ! slider-info-->
	</div>
	<section class="small-section">
		<div class="container">
			<div class="row">
				<div class="col-md-2 col-xs-6 mb-md-30">
					<div class="counter-block"><i class="counter-icon flaticon-suntour-world"></i>
						<div class="counter-name-wrap">
							<div data-count="345" class="counter">345</div>
							<div class="counter-name">Tours</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-xs-6 mb-md-30">
					<div class="counter-block with-divider"><i class="counter-icon flaticon-suntour-fireworks"></i>
						<div class="counter-name-wrap">
							<div data-count="438" class="counter">438</div>
							<div class="counter-name">Holidays</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-xs-6 mb-md-30">
					<div class="counter-block with-divider"><i class="counter-icon flaticon-suntour-hotel"></i>
						<div class="counter-name-wrap">
							<div data-count="526" class="counter">526</div>
							<div class="counter-name">Hotels</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-xs-6 mb-md-30">
					<div class="counter-block with-divider"><i class="counter-icon flaticon-suntour-ship"></i>
						<div class="counter-name-wrap">
							<div data-count="169" class="counter">169</div>
							<div class="counter-name">Cruises</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-xs-6">
					<div class="counter-block with-divider"><i class="counter-icon flaticon-suntour-airplane"></i>
						<div class="counter-name-wrap">
							<div data-count="293" class="counter">293</div>
							<div class="counter-name">Flights</div>
						</div>
					</div>
				</div>
				<div class="col-md-2 col-xs-6">
					<div class="counter-block with-divider"><i class="counter-icon flaticon-suntour-car"></i>
						<div class="counter-name-wrap">
							<div data-count="675" class="counter">675</div>
							<div class="counter-name">Cars</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- page section-->
	<section class="page-section pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<h6 class="title-section-top font-4">Special offers</h6>
					<h2 class="title-section"><span>Destinos</span>&nbsp;Más populares</h2>
					<div class="cws_divider mb-25 mt-5">
						<br>
					</div>
					<!--<p>
						Nullam ac dolor id nulla finibus pharetra. Sed sed placerat mauris. Pellentesque lacinia imperdiet interdum. Ut nec nulla in purus consequat lobortis. Mauris lobortis a nibh sed convallis.
					</p>-->
				</div>
				<div class="col-md-4">
					<img src="<?= base_url() ?>pic/promo-1.png" data-at2x="<?= base_url() ?>pic/promo-1@2x.png" alt="" class="mt-md-0 mt-minus-70">
				</div>
			</div>
		</div>
		<div class="features-tours-full-width">
			<div class="features-tours-wrap clearfix">
				<?php $this->
				db->select('destinos.*, grupos_destinos.nombre'); $this->db->join('grupos_destinos','grupos_destinos.id = destinos.grupos_destinos_id'); $this->db->limit('8'); $this->db->order_by('visitas','DESC'); $destinos = $this->db->get_where('destinos'); foreach($destinos->result() as $d): ?>
				<div class="features-tours-item">
					<div class="features-media">
						<img style="width: 480px;" src="<?=base_url('img/destinos/'.$d->portada_main) ?>" data-at2x="<?= base_url('img/destinos/'.$d->
						portada_main) ?>" alt="" data-mce-style="width: 480px;">
						<div class="features-info-top">
							<div class="info-price font-4">
								<span>Desde</span><?= moneda($d->precio) ?>€/pax
							</div>
							<div class="info-temp font-4">
								<span>A partir de </span><?= $d->
								pax ?> pax
							</div>
							<p class="info-text">
								<?= substr(strip_tags($d->
								descripcion_corta),0,255) ?>
							</p>
						</div>
						<div class="features-info-bot">
							<h4 class="title"><span class="font-4"><?= $d->
							nombre ?></span><?= $d->
							destinos_nombre ?></h4>
							<a href="<?=site_url('destino/'.toURL($d->id.'-'.$d->destinos_nombre)) ?>" class="button">Detalles</a><br>
						</div>
					</div>
				</div>
				<?php endforeach ?>
			</div>
		</div>
	</section><section class="small-section bg-gray">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h6 class="title-section-top font-4">LOS MEJORES</h6>
				<h2 class="title-section"><span>paquetes</span>&nbsp;recomendados</h2>
				<div class="cws_divider mb-25 mt-5">
					<br>
				</div>
				<!--<p>
					Maecenas commodo odio ut vulputate cursus. Integer in egestas lectus. Nam volutpat feugiat mi vitae mollis. Aenean tristique dolor bibendum mi scelerisque ultrices non at lorem.
				</p>-->
			</div>
			<div class="col-md-4">
				<i class="flaticon-suntour-hotel title-icon"></i><br>
			</div>
		</div>
		<div class="row">
			<?php $this->
			db->limit(6); $this->db->order_by('id','DESC'); $this->db->select('paquetes.*, grupos_destinos.nombre'); $this->db->join('grupos_destinos','grupos_destinos.id = paquetes.grupos_destinos_id'); $paquetes = $this->db->get('paquetes'); foreach($paquetes->result() as $p): ?>
			<div class="col-md-6">
				<div class="recom-item">
					<div class="recom-media">
						<div class="pic">
							<img src="<?=base_url('img/paquetes/'.$p->portada) ?>" data-at2x="<?= base_url('img/paquetes/'.$p->
							portada) ?>" alt="">
						</div>
						<div class="location">
							<?= $p->
							nombre ?>
						</div>
					</div>
					<!-- Recomended Content-->
					<div class="recom-item-body">
						<h6 class="blog-title"><?= $p->
						paquetes_nombre ?></h6>
						<div class="recom-price">
							<span class="font-4"><?= moneda($p->precio) ?>€/pax</span>
						</div>
						<p class="mb-30">
							<?= $p->
							descripcion_corta ?>
						</p>
						<a href="<?=site_url('paquete/'.toURL($p->id.'-'.$p->paquetes_nombre)) ?>" class="recom-button">Leer más</a><a href="<?=site_url('paquete/'.toURL($p->id.'-'.$p->paquetes_nombre)) ?>" class="cws-button small alt">RESERVAR</a><br>
					</div>
					<!-- Recomended Image-->
				</div>
			</div>
			<?php endforeach ?>
		</div>
	</div>
</section>
<!-- ! testimonials section-->
<section class="small-section cws_prlx_section bg-blue-40"><img src="<?= base_url() ?>pic/parallax-2.jpg" alt="" class="cws_prlx_layer">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h6 class="title-section-top font-4">Happy Memories</h6>
				<h2 class="title-section alt-2"><span>Our</span> Testimonials</h2>
				<div class="cws_divider mb-25 mt-5">
					<br>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- testimonial carousel-->
			<div class="owl-three-item">
				
				<?php foreach($this->db->get_where('testimonios')->result() as $t): ?>
					<!-- testimonial item-->
					<div class="testimonial-item">
						<div class="testimonial-top">
							<div class="pic">
								<img src="<?= base_url('img/testimonios/'.$t->foto_banner) ?>" alt="">
							</div>
							<div class="author">
								<img src="<?= base_url('img/testimonios/'.$t->foto_autor) ?>" alt="">
							</div>
						</div>
						<!-- testimonial content-->
						<div class="testimonial-body">
							<h5 class="title"><?= $t->autor ?></h5>
							<div class="stars stars-<?= $t->estrellas ?>">
								<br>
							</div>
							<p class="align-center">
								<?= $t->testimonio ?>
							</p>							
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</section>
<!-- gallery section-->
<section class="small-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<h6 class="title-section-top font-4">imágenes felices</h6>
				<h2 class="title-section"><span>GALERIA</span>&nbsp;DE FOTOS</h2>
				<div class="cws_divider mb-25 mt-5">
					<br>
				</div>
				<!--<p>
					Vestibulum feugiat vitae tortor ut venenatis. Sed cursus, purus eu euismod bibendum, diam nisl suscipit odio, vitae ultrices mauris dolor quis mauris. Curabitur ac metus id leo maximus porta.
				</p>-->
			</div>
			<div class="col-md-4">
				<i class="flaticon-suntour-photo title-icon"></i><br>
			</div>
		</div>
		<div class="row portfolio-grid">
			<!-- portfolio item-->
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="portfolio-item big">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/580x285-1.jpg" data-at2x="<?= base_url() ?>pic/portfolio/580x285-1@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/580x285-1@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="portfolio-item">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/285x285-1.jpg" data-at2x="<?= base_url() ?>pic/portfolio/285x285-1@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/285x285-1@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="portfolio-item">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/285x285-2.jpg" data-at2x="<?= base_url() ?>pic/portfolio/285x285-2@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/285x285-2@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="portfolio-item">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/285x285-3.jpg" data-at2x="<?= base_url() ?>pic/portfolio/285x285-3@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/285x285-3@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="portfolio-item">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/285x285-4.jpg" data-at2x="<?= base_url() ?>pic/portfolio/285x285-4@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/285x285-4@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="portfolio-item">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/285x285-5.jpg" data-at2x="<?= base_url() ?>pic/portfolio/285x285-5@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/285x285-5@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="portfolio-item">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/285x285-6.jpg" data-at2x="<?= base_url() ?>pic/portfolio/285x285-6@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/285x285-6@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="portfolio-item">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/285x285-7.jpg" data-at2x="<?= base_url() ?>pic/portfolio/285x285-7@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/285x285-7@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-3 col-sm-6 col-xs-6">
				<div class="portfolio-item">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/285x285-8.jpg" data-at2x="<?= base_url() ?>pic/portfolio/285x285-8@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/285x285-8@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
			<!-- portfolio item-->
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="portfolio-item big">
					<!-- portfolio image-->
					<div class="portfolio-media">
						<img src="<?= base_url() ?>pic/portfolio/580x285-2.jpg" data-at2x="<?= base_url() ?>pic/portfolio/580x285-2@2x.jpg" alt="">
					</div>
					<div class="links">
						<a href="<?= base_url() ?>pic/portfolio/580x285-2@2x.jpg" class="fancy"><i class="fa fa-expand"></i></a><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ! gallery section-->
<!-- latest news-->
<section class="small-section cws_prlx_section bg-blue-40" id="cws_prlx_section_200435880345"><img src="<?= base_url() ?>pic/parallax-3.jpg" alt="" class="cws_prlx_layer" id="cws_prlx_layer_640085073480">
	<div class="container">
		<div class="row mb-50">
			<div class="col-md-8">
				<h6 class="title-section-top font-4">ÚLTIMAS NOTICIAS</h6>
				<h2 class="title-section alt-2"><span>NUESTRO</span> Blog</h2>
				<div class="cws_divider mb-25 mt-5">
					<br>
				</div>
				<!--<p class="color-white">
					Vestibulum feugiat vitae tortor ut venenatis. Sed cursus, purus eu euismod bibendum, diam nisl suscipit odio, vitae ultrices mauris dolor quis mauris. Curabitur ac metus id leo maxim.
				</p>-->
			</div>
			<div class="col-md-4">
				<i class="flaticon-suntour-calendar title-icon alt"></i><br>
			</div>
		</div>
		<div class="carousel-container">
			<div class="row">
				<div class="owl-two-pag pagiation-carousel mb-20">
					<?php $this->
					db->limit('3'); $this->db->order_by('id','DESC'); $blog = $this->db->get_where('blog'); foreach($blog->result() as $b): ?>
					<!-- Blog item-->
					<div class="blog-item clearfix">
						<!-- Blog Image-->
						<div class="blog-media">
							<div class="pic">
								<img style="width: 270px; height: 270px;" src="<?=base_url('img/blog/'.$b->foto) ?>" data-at2x="<?= base_url('img/blog/'.$b->
								foto) ?>" alt="" data-mce-style="width: 270px; height: 270px;">
							</div>
						</div>
						<!-- blog body-->
						<div class="blog-item-body clearfix">
							<!-- title-->
							<h6 class="blog-title"><?= $b->
							titulo ?></h6>
							<div class="blog-item-data">
								<?= date("d-m-Y",strtotime($b->
								fecha)) ?>
							</div>
							<a href="blog-single.html">
								<!-- Text Intro-->
								<!-- Text Intro-->
							</a>
							<p>
								<?= substr(strip_tags($b->
								texto),0,90).'...' ?>
							</p>
							<a href="<?=site_url('blog/'.toURL($b->id.'-'.$b->titulo)) ?>" class="blog-button">Leer más</a><br>
						</div>
					</div>
					<!-- ! Blog item-->
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ! latest news-->
<!-- call out section-->
<?php $this->
load->view('includes/template/subscribe'); ?>
</div>