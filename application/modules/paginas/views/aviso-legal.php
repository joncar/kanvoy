    <header>
      <?php $this->load->view('includes/template/header'); ?> 
      <section style="background-image:url('<?= base_url() ?>pic/breadcrumbs/bg-1.jpg');" class="breadcrumbs">
        <div class="container">
          <div class="text-left breadcrumbs-item">          	
            <h2><span>Aviso legal</span></h2>
          </div>
        </div>
      </section>
      <!-- ! breadcrumbs end-->
    </header>
    <!-- ! header page-->
    <div class="content-body">
      <!-- page section about-->
      <section class="small-section cws_prlx_section bg-white-80 pb-0"><img src="<?= base_url() ?>pic/parallax-4.jpg" alt class="cws_prlx_layer">
        <div class="container">
          <div class="row">
            <div class="col-md-6 mb-md-60">
              <!-- section title-->
              
              <!-- ! section title-->
              <p class="mb-30">
              	<ul>
              		<li><b>Responsable: </b> KANVOY</li>
              		<li><b>Finalidad: </b>Gestión de datos del cliente para ofertas comerciales que sean de su interés.</li>
              		<li><b>Legitimación: </b>consentimiento de su interesado o del representante legal.</li>
              		<li><b>Destinatarios: </b>no se cederán datos a terceros salvo obligación legal.</li>
              		<li><b>Derechos: </b>podrá ejercitar los derechos de acceso, rectificación, supresión y portabilidad de sus datos, y la limitación u oposición de su tratamiento, a revocar el consentimiento prestado y a presentar reclamación ante la agencia española de protección de datos.</li>
              		<li><b>Información adicional: </b>toda la información facilitada podrá encontrarla ampliada en la información adicional que está a su disposición en la web <a href="http://www.kanvoy.es" style="text-decoration: underline;">www.kanvoy.es</a> o bien solicitarla a través de nuestro email <a href="mailto:info@kanvoy.com" style="text-decoration: underline;">info@kanvoy.com</a></li>
              	</ul>
          	  </p>
          	  
              <div class="cws_divider short mb-30"></div>              
            </div>
            <div class="col-md-6 flex-item-end hidden-xs" style="position: absolute;bottom: 0;right: 0;"><img src="<?= base_url() ?>pic/promo-2.png" alt class="mt-minus-100"></div>
          </div>
        </div>
      </section>
      
    </div>



</html>