<header><?php $this->load->view('includes/template/header'); ?> 
<!-- breadcrumbs start--><section style="background-image: url('<?= base_url() ?>pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;" class="breadcrumbs" data-mce-style="background-image: url('<?= base_url() ?>pic/breadcrumbs/bg-1.jpg'); background-repeat: no-repeat; background-position: center;">
<div class="container"><div class="text-left breadcrumbs-item"><a href="#">Inicio</a><i>/</i> <a href="#" class="last"><span>Blog</span></a><h2><span>NOTI</span>CIES</h2></div></div></section>
<!-- ! breadcrumbs end--></header>
<div class="content-body">
    <div class="container page">
      <div class="row masonry" style="position: relative; height: 1208px;">
        <div class="col-md-12" style="position: absolute; left: 0px; top: 0px;">
          <div class="row">
            <?php foreach($detail->result() as $d): ?>
                <!-- Blog Post-->
                <div class="col-lg-6 mb-30">
                  <!-- Blog item-->
                  <div class="blog-item clearfix border">
                    <!-- Blog Image-->
                    <div class="blog-media">
                        <a href="<?= $d->link ?>">
                        <div class="pic">
                            <img src="<?= $d->foto ?>" data-at2x="<?= $d->foto ?>" alt="" style="width:270px;">
                        </div>
                        </a>
                    </div>
                    <!-- blog body-->
                    <div class="blog-item-body clearfix">
                      <!-- title-->
                      <a href="<?= $d->link ?>">
                        <h6 class="blog-title">
                            <?= $d->titulo ?>
                        </h6>
                      </a>
                      <div class="blog-item-data"><?= date("D, d-m-Y",strtotime($d->fecha)) ?></div>
                      <!-- Text Intro-->
                      <p><?= substr(strip_tags($d->texto),0,80).'...' ?></p>
                      <a href="<?= $d->link ?>" class="blog-button">Leer más</a>
                    </div>
                  </div>
                  <!-- ! Blog item-->
                </div>
                <!-- ! Blog Post-->
            <?php endforeach ?>
          </div>
        </div>
      </div>
    </div>
</div>