DROP VIEW IF EXISTS view_ventas;
CREATE VIEW view_ventas as SELECT 
ventas.id,
ventas.transaccion,
GROUP_CONCAT(' ',CONCAT(ventas_pasajeros.nombre,' ',ventas_pasajeros.apellido)) as pasajeros,
productos.nombre,
GROUP_CONCAT(' ',ventas_pasajeros.paradas) AS paradas,
CONCAT(FORMAT(ventas_detalles.total,2,'de_DE'),'€') as Total,
ventas.fecha,
ventas.status
FROM ventas_detalles 
INNER JOIN ventas ON ventas.id = ventas_detalles.ventas_id 
INNER JOIN productos ON productos.id = ventas_detalles.productos_id
INNER JOIN ventas_pasajeros ON ventas_pasajeros.ventas_detalles_id = ventas_detalles.id
GROUP BY ventas_detalles.id
ORDER BY ventas.id DESC